<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "questions".
 *
 * @property int $id
 * @property string $question
 * @property int $q_types_id
 *
 * @property AnswerOptions[] $answerOptions
 */
class Questions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'questions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['question', 'q_types_id'], 'required'],
            [['q_types_id'], 'integer'],
            [['question'], 'string', 'max' => 1500],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question' => 'Question',
            'q_types_id' => 'Choose Question Category',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswerOptions()
    {
        return $this->hasMany(AnswerOptions::className(), ['question_id' => 'id']);
    }

    /**
     * get catagory
     * ->where("id=:id", [":id"=>$p->id])
     */

    public static function getHierarchy() {
        $options = [];
        $parents = QuestionType::find()->all();
        foreach($parents as $id => $p) {
            $children = SubQuestionType::find()->where("question_type_id=:id", [":id"=>$p->id])->all();

            $child_options = [];
            foreach($children as $child) {
                $child_options[$child->id] = $child->name;
            }
            $options[$p->name] = $child_options;
        }
        return $options;
    }

    public function getType()
    {
        /*return $this->hasOne(Category::className(), ['id' => 'category_id']);*/
        return $this->hasOne(QuestionType::className(),['id' =>'q_types_id']);
    }

}
