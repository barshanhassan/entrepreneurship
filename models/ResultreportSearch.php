<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Resultreport;

/**
 * ResultreportSearch represents the model behind the search form of `app\models\Resultreport`.
 */
class ResultreportSearch extends Resultreport
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'attempt_numbers'], 'integer'],
            [['result_number', 'aptitude', 'openness', 'conscientiousness', 'extroversion', 'greeableness'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Resultreport::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'attempt_numbers' => $this->attempt_numbers,
        ]);

        $query->andFilterWhere(['like', 'result_number', $this->result_number])
            ->andFilterWhere(['like', 'aptitude', $this->aptitude])
            ->andFilterWhere(['like', 'openness', $this->openness])
            ->andFilterWhere(['like', 'conscientiousness', $this->conscientiousness])
            ->andFilterWhere(['like', 'extroversion', $this->extroversion])
            ->andFilterWhere(['like', 'greeableness', $this->greeableness]);

        return $dataProvider;
    }
}
