<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "answer_options".
 *
 * @property int $id
 * @property string $options
 * @property string $status
 * @property int $question_id
 * @property string $rating
 *
 * @property Questions $question
 */
class AnswerOptions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'answer_options';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['options', 'rating'], 'required'],
            [['question_id'], 'integer'],
            [['options'], 'string', 'max' => 1500],
            [['status'], 'string', 'max' => 20],
            [['rating'], 'string', 'max' => 300],
            [['question_id'], 'exist', 'skipOnError' => true, 'targetClass' => Questions::className(), 'targetAttribute' => ['question_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'options' => 'Answer Options',
            'status' => 'Status',
            'question_id' => 'Question ID',
            'rating' => 'Rating',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(Questions::className(), ['id' => 'question_id']);
    }
}
