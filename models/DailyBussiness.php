<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "daily_bussiness".
 *
 * @property int $id
 * @property string $b_name
 * @property int $city_id
 * @property int $price
 * @property int $cat_id
 *
 * @property DailyCity $city
 * @property DailyBussinessCat $cat
 */
class DailyBussiness extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'daily_bussiness';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['b_name', 'city_id', 'price', 'cat_id'], 'required'],
            [['city_id', 'price', 'cat_id'], 'integer'],
            [['b_name'], 'string', 'max' => 255],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => DailyCity::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['cat_id'], 'exist', 'skipOnError' => true, 'targetClass' => DailyBussinessCat::className(), 'targetAttribute' => ['cat_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'b_name' => 'B Name',
            'city_id' => 'City ID',
            'price' => 'Price',
            'cat_id' => 'Cat ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(DailyCity::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCat()
    {
        return $this->hasOne(DailyBussinessCat::className(), ['id' => 'cat_id']);
    }
}
