<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post".
 *
 * @property int $id
 * @property string $name
 * @property string $post_text
 * @property int $created_by
 * @property int $updated_by
 * @property string $created_on
 * @property int $category
 * @property string $img
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'post_text'], 'required'],
            [['created_by', 'updated_by', 'category'], 'integer'],
            [['created_on'], 'safe'],
            [['img'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['post_text'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'post_text' => 'Post Text',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_on' => 'Created On',
            'category' => 'Category',
            'img' => 'Img',
        ];
    }
}
