<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bussiness".
 *
 * @property int $id
 * @property string $name
 * @property string $content
 * @property string $image
 * @property string $city
 */
class Bussiness extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bussiness';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'name', 'content', 'image', 'city'], 'required'],
            [['id'], 'integer'],
            [['content', 'image'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['city'], 'string', 'max' => 10000],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'content' => 'Content',
            'image' => 'Image',
            'city' => 'City',
        ];
    }
}
