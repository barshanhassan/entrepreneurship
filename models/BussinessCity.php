<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bussiness_city".
 *
 * @property int $id
 * @property string $bussiness_name
 * @property double $latitude
 * @property double $langitutde
 *
 * @property BussinessCityChild[] $bussinessCityChildren
 */
class BussinessCity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bussiness_city';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bussiness_name', 'latitude', 'langitutde'], 'required'],
            [['latitude', 'langitutde'], 'number'],
            [['bussiness_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bussiness_name' => 'Bussiness Name',
            'latitude' => 'Latitude',
            'langitutde' => 'Langitutde',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBussinessCityChildren()
    {
        return $this->hasMany(BussinessCityChild::className(), ['bussiness_city_id' => 'id']);
    }
}
