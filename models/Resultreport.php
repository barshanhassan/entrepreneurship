<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "resultreport".
 *
 * @property int $id
 * @property int $user_id
 * @property int $attempt_numbers
 * @property string $result_number
 * @property string $aptitude
 * @property string $openness
 * @property string $conscientiousness
 * @property string $extroversion
 * @property string $greeableness
 *
 * @property User $user
 */
class Resultreport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'resultreport';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'attempt_numbers'], 'required'],
            [['user_id', 'attempt_numbers'], 'integer'],
           // [['result_number', 'aptitude', 'openness', 'conscientiousness', 'extroversion', 'greeableness'], 'string', 'max' => 255],
           // [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            //'id' => 'ID',
            'user_id' => 'User Name',
            'attempt_numbers' => 'Attempt Numbers',
            'result_number' => 'OverAll %age',
            'aptitude' => 'Aptitude %age',
            'openness' => 'Openness %age',
            'conscientiousness' => 'Conscientiousness %',
            'extroversion' => 'Extroversion %age',
            'greeableness' => 'Greeableness %age',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
