<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "daily_city".
 *
 * @property int $id
 * @property string $city_name
 *
 * @property DailyBussiness[] $dailyBussinesses
 */
class DailyCity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'daily_city';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city_name'], 'required'],
            [['city_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_name' => 'City Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDailyBussinesses()
    {
        return $this->hasMany(DailyBussiness::className(), ['city_id' => 'id']);
    }
}