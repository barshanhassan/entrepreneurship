<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bussiness_city_child".
 *
 * @property int $id
 * @property int $bussiness_city_id
 * @property string $city_child_name
 * @property int $bussiness_name_id
 * @property double $longitude
 * @property double $latitude
 *
 * @property BussinessCity $bussinessCity
 * @property BussinessName $bussinessName
 */
class BussinessCityChild extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bussiness_city_child';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bussiness_city_id', 'city_child_name', 'bussiness_name_id', 'longitude', 'latitude'], 'required'],
            [['bussiness_city_id', 'bussiness_name_id'], 'integer'],
            [['longitude', 'latitude'], 'number'],
            [['city_child_name'], 'string', 'max' => 255],
            [['bussiness_city_id'], 'exist', 'skipOnError' => true, 'targetClass' => BussinessCity::className(), 'targetAttribute' => ['bussiness_city_id' => 'id']],
            [['bussiness_name_id'], 'exist', 'skipOnError' => true, 'targetClass' => BussinessName::className(), 'targetAttribute' => ['bussiness_name_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bussiness_city_id' => 'Select Main City',
            'city_child_name' => 'Enter Area Name',
            'bussiness_name_id' => 'Select Product',
            'longitude' => 'Enter Longitude',
            'latitude' => 'Enter Latitude',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBussinessCity()
    {
        return $this->hasOne(BussinessCity::className(), ['id' => 'bussiness_city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBussinessName()
    {
        return $this->hasOne(BussinessName::className(), ['id' => 'bussiness_name_id']);
    }
}