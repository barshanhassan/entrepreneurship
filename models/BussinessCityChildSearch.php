<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BussinessCityChild;

/**
 * BussinessCityChildSearch represents the model behind the search form of `app\models\BussinessCityChild`.
 */
class BussinessCityChildSearch extends BussinessCityChild
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'bussiness_city_id', 'bussiness_name_id'], 'integer'],
            [['city_child_name'], 'safe'],
            [['longitude', 'latitude'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BussinessCityChild::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'bussiness_city_id' => $this->bussiness_city_id,
            'bussiness_name_id' => $this->bussiness_name_id,
            'longitude' => $this->longitude,
            'latitude' => $this->latitude,
        ]);

        $query->andFilterWhere(['like', 'city_child_name', $this->city_child_name]);

        return $dataProvider;
    }
}
