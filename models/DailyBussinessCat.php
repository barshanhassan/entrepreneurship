<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "daily_bussiness_cat".
 *
 * @property int $id
 * @property string $name
 *
 * @property DailyBussiness[] $dailyBussinesses
 */
class DailyBussinessCat extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'daily_bussiness_cat';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 11],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDailyBussinesses()
    {
        return $this->hasMany(DailyBussiness::className(), ['cat_id' => 'id']);
    }
}
