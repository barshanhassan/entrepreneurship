<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "test".
 *
 * @property int $id
 * @property int $user_id
 * @property int $questions_id
 * @property int $question_type_id
 * @property int $answer_option_id
 * @property int $sub_q_type_id
 */
class Test extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'test';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'questions_id', 'question_type_id', 'answer_option_id', 'sub_q_type_id'], 'integer'],
            [['answer_option_id'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'questions_id' => 'Questions ID',
            'question_type_id' => 'Question Type ID',
            'answer_option_id' => 'Answer Option ID',
            'sub_q_type_id' => 'Sub Q Type ID',
        ];
    }
}
