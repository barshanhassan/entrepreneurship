<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bussiness_name".
 *
 * @property int $id
 * @property string $bussiness_name
 *
 * @property BussinessCityChild[] $bussinessCityChildren
 */
class BussinessName extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bussiness_name';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bussiness_name'], 'required'],
            [['bussiness_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bussiness_name' => 'Enter Product Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBussinessCityChildren()
    {
        return $this->hasMany(BussinessCityChild::className(), ['bussiness_name_id' => 'id']);
    }
}
