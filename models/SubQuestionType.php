<?php

namespace app\models;

use app\controllers\QuestionTypeController;
use Yii;

/**
 * This is the model class for table "sub_question_type".
 *
 * @property int $id
 * @property int $question_type_id
 * @property string $name
 */
class SubQuestionType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sub_question_type';
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['question_type_id', 'name'], 'required'],
            [['question_type_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question_type_id' => 'Question Major Category',
            'name' => 'Question Sub Category',
        ];
    }
    public function getType()
    {
        /*return $this->hasOne(Category::className(), ['id' => 'category_id']);*/
        return $this->hasOne(QuestionType::className(),['id' =>'question_type_id']);
    }
}