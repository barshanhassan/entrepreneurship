/**
 * Created by MCE-PC on 05-Jun-17.
 */
$(document).ready(function() {
    $('#sales_grid').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'copy',
                footer:true
            },

            {
                extend: 'csv',
                footer:true
            },
            {
                extend: 'excel',
                footer:true
            },
            {
                extend: 'print',
                footer:true
            },
            {
                extend: 'pdfHtml5',
               // orientation: 'landscape',
                pageSize: 'LEGAL'
                //footer:true
            }
        ]
        /*"footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
            var col=[7,9,10,12,13,15,16,17,18];
            for(var i=0; i<col.length;i++)
            {
                // Total over all pages
                total = api
                    .column( col[i] )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Total over this page
                pageTotal = api
                    .column( col[i], { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer
                $( api.column(col[i] ).footer() ).html(total
                );
            }
        }*/
    } );

} );
