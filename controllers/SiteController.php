<?php

namespace app\controllers;



use app\models\User;
use app\models\Test;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\widgets\ActiveForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
   
     public function behaviors()
    {

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'actions' => ['login'],
                    'allow' => true,
                ],
                [
                    'actions' => ['signup'],
                    'allow' => true,
                ],
                [
                    'actions' => ['index'],
                    'allow' => true,
                ],
                [
                    'actions' => ['bussiness'],
                    'allow' => true,
                ],
                [
                    'actions' => ['bussinessinfo'],
                    'allow' => true,
                ],
                [
                    'actions' => ['successful'],
                    'allow' => true,
                ],
                [
                    'actions' => ['vegitables'],
                    'allow' => true,
                ],
                [
                    'actions' => ['desiproducts'],
                    'allow' => true,
                ],
                [
                    'actions' => ['freshmilk'],
                    'allow' => true,
                ],
                [
                    'actions' => ['mobilegarage'],
                    'allow' => true,
                ],
                [
                    'actions' => ['autorepair'],
                    'allow' => true,
                ],
                [

                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'logout' => ['GET'],
            ]
        ];


        return $behaviors;
    }


    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    /**
     * Login action.
     *
     * @return Response|string
     */

    public function actionLogin()
    {

        $this->layout ='login';

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            return $this->goBack();
        }
        $model2 = new User();
        $model2->scenario = 'create';

        if(Yii::$app->request->isAjax && $model2->load(Yii::$app->request->post())){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model2);
        }
        else  {

            if ($model2->load(Yii::$app->request->post())) {
                if ($user = $model2->signup()) {
                    if (Yii::$app->getUser()->login($user)) {

                            return $this->goBack();
                    }
                }
            }
            //$model->password = '';
            return $this->render('login', [
                'model' => $model,
                'model2'=> $model2,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');
            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }


    public function actionTest()
    {
        $this->layout = 'new_layout';
        return $this->render('test');
    }

    public function actionSubmit()
    {
        $user_id = Yii::$app->user->id;
        /*$table = 'TEST';
        $sql2= "UPDATE '".$table."' SET  status = 0 WHERE status = 1 and user_id= '".$user_id." ";
        Yii::$app->db->createCommand($sql2)->queryAll();*/

       // Yii::$app->db->createCommand("UPDATE test SET status=0 WHERE id= $user_id  and status =1")->execute();
        Yii::$app->db->createCommand()->update('test', ['status' => 0])->execute();
        $question = \app\models\Questions::find()->all();
        foreach($question as $res)
        {
            if(!empty($_POST['question'][$res['id']])) {
                $model1 = new Test();
                $model1->user_id = $user_id;
                $model1->questions_id = $_POST['question'][$res['id']];
                $model1->question_type_id = $_POST['type'][$res['id']];
                $model1->sub_q_type_id = $_POST['type'][$res['id']];
                $model1->answer_option_id = $_POST['subcatradio'][$res['id']];
                if ($model1->save()) {
                }
                else {

                    print_r($model1->getErrors());
                }
            }
        }
        return $this->render('result');
    }

    //action signup
    public function actionSign()
    {

    }
    public function actionUpdateuser()
    {

        return $this->render('updateuser');
    }
    public function actionSubmitupdateuser()
    {

        $user = User::findOne(Yii::$app->user->id);
        $user_id = Yii::$app->user->id;

       // $user->attempt_status = 1;
        $user->email = $_POST['email'];
         $user->phone = $_POST['phone'];
        $user->age = $_POST['age'];
        $user->gender= $_POST['gender'];
        $user->qualification = $_POST['qualification'];
        $user->addres = $_POST['address'];
        $user->city = $_POST['city'];
        $user->country = $_POST['country'];
        $user->qualification = $_POST['qualification'];
        $user->user_type = $user['user_type'];

       // $user->save();

        if($user->save())
        {
            return $this->render('updateuser');
        }
        else {
            print_r($user->getErrors());
            exit;
        }
    }
     public function actionChangepass(){
        return $this->render('changepass');
     }
     public function actionUpdatechangepass(){

          $pass = $_POST['mainpassword'];
         //$pass = $_POST['rpassword'];
         $password_hash = Yii::$app->getSecurity()->generatePasswordHash($pass);
         $user = User::findOne(Yii::$app->user->id);
         $user->email = $user['email'];
         $user->phone = $user['phone'];
         $user->age = $user['age'];
         $user->gender = $user['gender'];
         $user->addres = $user['addres'];
         $user->city = $user['city'];
         $user->qualification = $user['qualification'];
         $user->user_type = $user['user_type'];
         $user->password_hash = $password_hash;
         if($user->save())
         {
             return $this->render('changepass');
         }
     }

     public function actionBussiness(){

        return $this->render('bussiness');
     }

    public function actionBussinessinfo(){

        return $this->render('bussinessinfo');
    }

    public function actionUserreport(){
        return $this->render('userreport');
    }

    public function actionSuccessful(){
        return $this->render('successful_enterpenure');
    }

    public function actionVegitables(){
        return $this->render('vegitables');
    }

    public function actionDesiproducts(){
        return $this->render('desiproducts');
    }
    public function actionFreshmilk(){
        return $this->render('freshmilk');
    }
    public function actionMobilegarage(){
        return $this->render('mobilegarage');
    }

    public function actionAutorepair(){
        return $this->render('autorepair');
    }

    public function actionTestreport(){
        return $this->render('testreport');
    }
    public function actionCreateuser(){
    return $this->render('createuser');
}
}
