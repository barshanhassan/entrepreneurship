<?php


namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LoginAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all',
        'src/assets/global/plugins/font-awesome/css/font-awesome.min.css',
        'src/assets/global/plugins/simple-line-icons/simple-line-icons.min.css',
        'src/assets/global/plugins/bootstrap/css/bootstrap.min.css',
        'src/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
        'src/assets/global/plugins/select2/css/select2.min.css',
        'src/assets/global/plugins/select2/css/select2-bootstrap.min.css',
        'src/assets/global/css/components.min.css',
        'src/assets/global/css/plugins.min.css',
        'src/assets/pages/css/login.min.css',
        //'src//assets/global/plugins/icheck/skins/all.css'
    ];
    public $js = [

        //'src/assets/global/plugins/jquery.min.js',
        'src/assets/global/plugins/bootstrap/js/bootstrap.min.js',
        'src/assets/global/plugins/js.cookie.min.js',
        'src/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
        'src/assets/global/plugins/jquery.blockui.min.js',
        'src/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
        'src/assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
        'src/assets/global/plugins/jquery-validation/js/additional-methods.min.js',
        'src/assets/global/plugins/select2/js/select2.full.min.js',
        'src/assets/global/scripts/app.min.js',
        'src/assets/pages/scripts/login.min.js',
        'src/assets/global/plugins/icheck/icheck.min.js',






    ];
    public $depends = [



    ];
}
