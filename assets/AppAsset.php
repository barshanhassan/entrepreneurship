<?php


namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all',
        'src/assets/global/plugins/font-awesome/css/font-awesome.min.css',
        'src/assets/global/plugins/simple-line-icons/simple-line-icons.min.css',
        'src/assets/global/plugins/bootstrap/css/bootstrap.min.css',
        'src/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
        'src/assets/global/plugins/datatables/datatables.min.css',
        'src/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
        'src/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.css',
        'src/assets/global/plugins/fullcalendar/fullcalendar.css',
        'src/assets/global/plugins/jqvmap/jqvmap/jqvmap.css',
        'src/assets/global/css/components.css',
        'src/assets/global/css/plugins.css',
        'src/assets/layouts/layout3/css/layout.min.css',
        'src/assets/layouts/layout3/css/themes/default.min.css',
        'src/assets/layouts/layout3/css/custom.min.css',
        'src/assets/global/plugins/select2/css/select2.min.css',
        'src/assets/global/plugins/select2/css/select2-bootstrap.min.css',
		'src/assets/global/css/new-custom.css',
		'src/assets/global/css/flaticon.css',
		
        //'js/tiny/css/style.css',



    ];
    public $js = [

        'src/assets/global/plugins/bootstrap/js/bootstrap.min.js',
        'src/assets/global/plugins/js.cookie.min.js',
        'src/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
        'src/assets/global/plugins/jquery.blockui.min.js',
        'src/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
        'src/assets/global/scripts/datatable.js',
        'src/assets/global/plugins/datatables/datatables.min.js',
        'src/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js',
        'src/assets/global/plugins/moment.min.js',
        'src/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js',
        'src/assets/global/plugins/morris/morris.min.js',
        'src/assets/global/plugins/morris/raphael-min.js',

        'src/assets/global/plugins/flot/jquery.flot.min.js',
        'src/assets/global/plugins/flot/jquery.flot.resize.min.js',
        'src/assets/global/plugins/flot/jquery.flot.categories.min.js',
        'src/assets/global/plugins/jquery.pulsate.min.js',

        'src/assets/global/scripts/app.min.js',
        'src/assets/layouts/layout3/scripts/layout.min.js',
        'src/assets/layouts/layout3/scripts/demo.min.js',
        'src/assets/layouts/global/scripts/quick-sidebar.min.js',
        'src/assets/layouts/global/scripts/quick-nav.min.js',
        'src/assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
        'src/assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js',
        'js/custom.js',
        'src/assets/global/plugins/select2/js/select2.full.min.js',
        //'http://code.jquery.com/jquery-1.12.4.min.js',
        'js/datatablescrpt.js'

    ];
    public $depends = [

    ];
}