<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AnswerOptions */

$this->title = 'Create Answer Options';
$this->params['breadcrumbs'][] = ['label' => 'Answer Options', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
<div class="answer-options-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>