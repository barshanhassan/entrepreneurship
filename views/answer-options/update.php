<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AnswerOptions */

$this->title = 'Update Answer Options: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Answer Options', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="container-fluid">
<div class="answer-options-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>