<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AnswerOptions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="answer-options-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'options')->textInput(['maxlength' => true]) ?>


    <?=$form->field($model,'rating')->dropDownList(array_combine(range(1,100),range(1,100)),['prompt'=>'Select Rating....']) ?>


    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
