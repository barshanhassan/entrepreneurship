<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DailyBussinessCat */

$this->title = 'Create Daily Bussiness Cat';
$this->params['breadcrumbs'][] = ['label' => 'Daily Bussiness Cats', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
<div class="daily-bussiness-cat-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>