<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DailyBussinessCat */

$this->title = 'Update Daily Bussiness Cat: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Daily Bussiness Cats', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="container-fluid">
<div class="daily-bussiness-cat-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>