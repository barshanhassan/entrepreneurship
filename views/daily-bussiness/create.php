<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DailyBussiness */

$this->title = 'Create Daily Bussiness';
$this->params['breadcrumbs'][] = ['label' => 'Daily Bussinesses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
<div class="daily-bussiness-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>