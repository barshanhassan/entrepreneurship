<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\DailyBussiness */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="daily-bussiness-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'b_name')->textInput(['maxlength' => true]) ?>


    <?=  $form->field($model, 'cat_id')->dropDownList(ArrayHelper::map(\app\models\dailyBussinessCat::find()->all(),'id','name'),['prompt'=>'Select Category']); ?>

    <?=  $form->field($model, 'city_id')->dropDownList(ArrayHelper::map(\app\models\dailyCity::find()->all(),'id','city_name'),['prompt'=>'Select City']); ?>


    <?= $form->field($model, 'price')->textInput() ?>



    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
