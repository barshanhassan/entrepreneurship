<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DailyBussiness */

$this->title = 'Update Daily Bussiness: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Daily Bussinesses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="container-fluid">
<div class="daily-bussiness-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>