<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\QuestionTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Question Major Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
<div class="question-type-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Question Major Category', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn',
                'header' => 'Serial #',

            ],

            'name',

            [
                'class' => 'yii\grid\ActionColumn',
                  'header' => 'Actions',

            ],
        ],
    ]); ?>

</div>
</div>