<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Resultreport */

$this->title = 'Create Resultreport';
$this->params['breadcrumbs'][] = ['label' => 'Resultreports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="resultreport-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
