<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Resultreport */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="resultreport-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'attempt_numbers')->textInput() ?>

    <?= $form->field($model, 'result_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'aptitude')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'openness')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'conscientiousness')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'extroversion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'greeableness')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
