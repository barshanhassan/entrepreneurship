<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ResultreportSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="resultreport-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'attempt_numbers') ?>

    <?= $form->field($model, 'result_number') ?>

    <?= $form->field($model, 'aptitude') ?>

    <?php // echo $form->field($model, 'openness') ?>

    <?php // echo $form->field($model, 'conscientiousness') ?>

    <?php // echo $form->field($model, 'extroversion') ?>

    <?php // echo $form->field($model, 'greeableness') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
