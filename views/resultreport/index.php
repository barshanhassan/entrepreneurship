<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ResultreportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Resultreports';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
<div class="resultreport-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Resultreport', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


            'user_id',
            ['attribute' => 'user_id',
                'value' => 'user.username'],
           // 'attempt_numbers',
            //['attribute'=> 'aptitude',
             //   'value'=>'aptitude . "kk"',],

            'openness',
            'conscientiousness',
            'extroversion',
            'greeableness',
            'result_number',
           // ['class' => 'yii\grid\ActionColumn',
           // 'visible'=> 'false'],
        ],
    ]); ?>
</div>
</div>