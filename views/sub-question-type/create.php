<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SubQuestionType */

$this->title = 'Create Question Sub Category';
$this->params['breadcrumbs'][] = ['label' => 'Question Sub Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
<div class="sub-question-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>