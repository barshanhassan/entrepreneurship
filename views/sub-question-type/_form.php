<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\SubQuestionType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sub-question-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo  $form->field($model, 'question_type_id')->dropDownList(ArrayHelper::map(\app\models\QuestionType::find()->all(),'id','name'),['prompt'=>'Select Type']);
    ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
