<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SubQuestionTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="container-fluid">
<?php
$this->title = 'Question Sub Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="container-fluid">
<div class="sub-question-type-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Question Sub Category', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [   'class' => 'yii\grid\SerialColumn',
                'header' =>'Serial #',
                'contentOptions' => ['style' => 'width:70px;'],
            ],
            [
                    'attribute'=>'question_type_id',
                    'value'=>'type.name',
            ],
            'name',
            [
                    'header' => 'Actions',
                    'class' => 'yii\grid\ActionColumn'
            ],
        ],
    ]); ?>
</div>
</div>
</div>