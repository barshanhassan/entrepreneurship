<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\models\User;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script src="<?= Yii::$app->homeUrl?>src/assets/global/plugins/jquery.min.js"></script>
</head>
<body class="page-container-bg-solid">
<?php $this->beginBody() ?>
<div class="page-wrapper">
    <div class="page-wrapper-row">
        <div class="page-wrapper-top">
            <!-- BEGIN HEADER -->
            <div class="page-header">
                <!-- BEGIN HEADER TOP -->
                <div class="page-header-top">
                    <div class="container">
                        <!-- BEGIN LOGO -->
                        <div class="page-logo">
                            <a href="http://localhost/fyp/web/">
                                <img src="<?= Yii::$app->homeUrl?>src/assets/C2.PNG" alt="logo" style="margin-top:  10px; width: 190px;height: 60px;">
                            </a>
                        </div>
                        <!-- END LOGO -->
                        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                        <a href="javascript:;" class="menu-toggler"></a>
                        <!-- END RESPONSIVE MENU TOGGLER -->
                        <!-- BEGIN TOP NAVIGATION MENU -->
                        <div class="top-menu">
                            <ul class="nav navbar-nav pull-right">
                                <?php
                                if (Yii::$app->user->isGuest) {?>
                                    <a href="<?= Yii::$app->homeUrl?>site/login" class="btn btn-success">Sign Up</a>
                                    <a href="<?= Yii::$app->homeUrl?>site/login" class="btn btn-primary">Login</a>
                                <?php   }else {
                                    ?>
                                    <!-- BEGIN USER LOGIN DROPDOWN -->
                                    <li class="dropdown dropdown-user dropdown-dark">
                                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                            <img alt="" class="img-circle" src="../assets/layouts/layout3/img/avatar9.jpg">
                                            <i class="icon-user"></i>
                                            <span class="username username-hide-mobile">
                                                    <?= Yii::$app->user->identity->username?>
                                                </span>
                                        </a>
                                        <ul class="dropdown-menu pull-left">
                                            <li aria-haspopup="true" class="">
                                                <a href="<?= Yii::$app->homeUrl?>site/updateuser" class="nav-link nav-toggle ">
                                                    <i class=""></i> Update User
                                                    <span class="arrow"></span>
                                                </a>
                                            </li>
                                            <li aria-haspopup="true" class="">
                                                <a href="<?= Yii::$app->homeUrl?>site/changepass" class="nav-link nav-toggle ">
                                                    <i class=""></i> Change Password
                                                    <span class="arrow"></span>
                                                </a>
                                            </li>
                                            <li aria-haspopup="true" class="">
                                                <a href="<?= Yii::$app->homeUrl?>site/logout" class="nav-link nav-toggle ">
                                                    <i class=""></i> Logout
                                                    <span class="arrow"></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                <?php } ?>
                                <!-- END USER LOGIN DROPDOWN -->
                                <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                                <!-- END QUICK SIDEBAR TOGGLER -->
                            </ul>
                        </div>
                        <!-- END TOP NAVIGATION MENU -->
                    </div>
                </div>
                <!-- END HEADER TOP -->
                <!-- BEGIN HEADER MENU -->
                <div class="page-header-menu">
                    <div class="container-fluid">
                        <!-- BEGIN HEADER SEARCH BOX -->
                        <!-- <form class="search-form" action="page_general_search.html" method="GET">
                             <div class="input-group">
                                 <input type="text" class="form-control" placeholder="Search" name="query">
                                 <span class="input-group-btn">
                                     <a href="javascript:;" class="btn submit">
                                         <i class="icon-magnifier"></i>
                                     </a>
                                 </span>
                             </div>
                         </form>-->
                        <!-- END HEADER SEARCH BOX -->
                        <!-- BEGIN MEGA MENU -->
                        <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
                        <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
                        <div class="hor-menu">
                            <ul class="nav navbar-nav">
                                <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown active">
                                    <a href="<?= Yii::$app->homeUrl?>"> HOME
                                        <span class="arrow"></span>
                                    </a>
                                </li>
                                <li aria-haspopup="true" class="menu-dropdown mega-menu-dropdown">
                                    <a href="<?= Yii::$app->homeUrl?>site/test/"> TAKE TEST
                                        <span class="arrow"></span>
                                    </a>
                                </li>
                                <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown">
                                    <a href="<?= Yii::$app->homeUrl?>site/bussiness"> BUSSINESS
                                        <span class="arrow"></span>
                                    </a>
                                </li>
                                <li aria-haspopup="true" class="menu-dropdown mega-menu-dropdown  mega-menu-full">
                                    <a href="<?=Yii::$app->homeUrl?>site/bussinessinfo"> BUSSINESS INFORMATION
                                        <span class="arrow"></span>
                                    </a>
                                </li>
                                <li aria-haspopup="true" class="menu-dropdown mega-menu-dropdown  mega-menu-full">
                                    <a href="<?=Yii::$app->homeUrl?>site/successful"> SUCCESSFUL STORIES
                                        <span class="arrow"></span>
                                    </a>
                                </li>
                                <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown ">
                                    <a href="<?= Yii::$app->homeUrl ?>daily-bussiness/index"> DAILY NEWS
                                        <span class="arrow"></span>
                                    </a>
                                </li>
                                <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown ">
                                    <?php
                                    if(Yii::$app->user->id==1)
                                    {
                                        ?>
                                        <a href="javascript:;">
                                            Settings
                                            <span class="arrow"></span>
                                        </a>
                                    <?php }?>
                                    <ul class="dropdown-menu pull-left">
                                        <li aria-haspopup="true" class="">
                                            <a href="<?= Yii::$app->homeUrl?>questions/" class="nav-link nav-toggle ">
                                                <i class=""></i> Create Questions
                                                <span class="arrow"></span>
                                            </a>
                                        </li>
                                        <li aria-haspopup="true" class="">
                                            <a href="<?= Yii::$app->homeUrl?>sub-question-type/" class="nav-link nav-toggle ">
                                                <i class=""></i> Create Question Sub-Catagory
                                                <span class="arrow"></span>
                                            </a>
                                        </li>

                                        <li aria-haspopup="true" class="">
                                            <a href="<?= Yii::$app->homeUrl?>question-type/" class="nav-link nav-toggle ">
                                                <i class=""></i> Create Question Catagory
                                                <span class="arrow"></span>
                                            </a>
                                        </li>

                                        <li aria-haspopup="true" class="">
                                            <a href="<?= Yii::$app->homeUrl?>answer-options/" class="nav-link nav-toggle ">
                                                <i class=""></i> Create Answer Option
                                                <span class="arrow"></span>
                                            </a>
                                        </li>
                                        <li aria-haspopup="true" class="">
                                            <a href="<?= Yii::$app->homeUrl?>post/create" class="nav-link nav-toggle ">
                                                <i class=""></i> Create Post
                                                <span class="arrow"></span>
                                            </a>
                                        </li>
                                        <li aria-haspopup="true" class="">
                                            <a href="<?= Yii::$app->homeUrl?>post-category/create" class="nav-link nav-toggle ">
                                                <i class=""></i> Create Post Category
                                                <span class="arrow"></span>
                                            </a>
                                        </li>
                                        <li aria-haspopup="true" class="">
                                            <a href="<?= Yii::$app->homeUrl?>daily-city/create" class="nav-link nav-toggle ">
                                                <i class=""></i> Create bussiness city
                                                <span class="arrow"></span>
                                            </a>
                                        </li>
                                        <li aria-haspopup="true" class="">
                                            <a href="<?= Yii::$app->homeUrl?>daily-bussiness/create" class="nav-link nav-toggle ">
                                                <i class=""></i> Create Bussiness
                                                <span class="arrow"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!-- END MEGA MENU -->
                    </div>
                </div>
                <!-- END HEADER MENU -->
            </div>
            <!-- END HEADER -->
        </div>
    </div>
    <div class="page-wrapper-row full-height">
        <div class="page-wrapper-middle">
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">

                    <?= $content ?>
                </div>
            </div>
        </div>
    </div>

</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
