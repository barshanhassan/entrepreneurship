<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\models\User;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script src="<?= Yii::$app->homeUrl?>src/assets/global/plugins/jquery.min.js"></script>
</head>
<body class="page-container-bg-solid">
<?php $this->beginBody() ?>
<div class="page-wrapper">
     <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <!-- BEGIN HEADER -->
                    <div class="page-header">
						<div class="maintop">
							<div class="container">
								<div class="maintop-left maintop-widgets text-left">
                                    <div class="widget">
                                        <div class="office-location clearfix">
                                            <ul class="maintop-office">
                                                <li class="phone"><i class="flaticon-telephone" aria-hidden="true"></i>+92 321 616 6195</li>
                                                <li class="email"><i class="flaticon-web" aria-hidden="true"></i>iamshahzaib89@gmail.com,</li>
                                                <li class="address"><i class="flaticon-pin" aria-hidden="true"></i>Gujrat, Pakistan</li>
                                            </ul>

                                        </div>
                                    </div>
							    </div><!-- maintop-left maintop-widgets text-left Close-->
                                <div class="maintop-right maintop-widgets text-right">
                                    <div class="widget social-links-widget">
                                        <div class="list-social style-1">
                                            <a href="#" class="share-facebook tooltip-enable social" rel="nofollow" title="Facebook" data-toggle="tooltip" data-placement="top" target="_blank">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                            <a href="#" class="share-twitter tooltip-enable social" rel="nofollow" title="Twitter" data-toggle="tooltip" data-placement="top" target="_blank">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                            <a href="#" class="share-google-plus tooltip-enable social" rel="nofollow" title="Google Plus" data-toggle="tooltip" data-placement="top" target="_blank">
                                                <i class="fa fa-google-plus"></i>
                                            </a>
                                            <a href="#" class="share-skype tooltip-enable social" rel="nofollow" title="Skype" data-toggle="tooltip" data-placement="top" target="_blank">
                                                <i class="fa fa-skype"></i>
                                            </a>
                                            <a href="#" class="share-youtube tooltip-enable social" rel="nofollow" title="Youtube" data-toggle="tooltip" data-placement="top" target="_blank">
                                                <i class="fa fa-youtube"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
							</div><!-- Container -->
                        </div><!-- Close Maintop -->
					


                        <!-- BEGIN HEADER TOP -->
                        <div class="page-header-top">
                            <div class="container">
									 <!-- new Mid header -->
										<header class="site-header clearfix">
											<div class="header-main clearfix">
												<div class="container mobile_relative">		
													<div class="row">
														<div class="site-logo col-md-3 col-sm-6 col-xs-6">
															<a href="#" class="logo">
																<img src="<?= Yii::$app->homeUrl?>src/assets/Logo-Enterpenure.jpg" alt="logo" style="">
															</a>
                                                        </div>
														
														
														<div class="site-menu col-md-7 col-sm-6 col-xs-6">
                                                            <div class="page-header-menu" style="display: block;">
                       
									<div class="hor-menu">
                                    <ul class="nav navbar-nav">
                                        <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown">
                                            <a href="<?= Yii::$app->homeUrl?>"> Home
                                                <span class="arrow"></span>
                                            </a>
                                        </li>
                                        <li aria-haspopup="true" class="menu-dropdown mega-menu-dropdown">
                                            <a href="<?= Yii::$app->homeUrl?>site/test/" target="_blank"> Take Test
                                                <span class="arrow"></span>
                                            </a>
                                        </li>
										
										<li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown ">
                                             <a href="#">
                                                 Business
                                                <span class="arrow"></span>
                                            </a>
                                                  <ul class="dropdown-menu pull-left">
												  
                                                      <li aria-haspopup="true" class="">
															<a href="<?= Yii::$app->homeUrl?>site/bussiness" class="nav-link nav-toggle ">
															<i class=""></i> Business Locator
															<span class="arrow"></span>
															</a>
                                                       </li>
                                                      <li aria-haspopup="true" class="">
															<a href="<?= Yii::$app->homeUrl?>site/bussinessinfo" class="nav-link nav-toggle ">
															<i class=""></i> Business Info
															<span class="arrow"></span>
															</a>
                                                      </li>
                                                  </ul>

                                        </li>



                                        <!--<li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown">
                                            <a href="/fyp/web/site/bussiness"> BUSSINESS
                                                <span class="arrow"></span>
                                            </a>

                                        </li>-->
                                        <!--<li aria-haspopup="true" class="menu-dropdown mega-menu-dropdown  mega-menu-full">
                                            <a href="/fyp/web/site/bussinessinfo"> BUSSINESS INFORMATION
                                                <span class="arrow"></span>
                                            </a>
                                        </li>-->
                                        <li aria-haspopup="true" class="menu-dropdown mega-menu-dropdown  mega-menu-full">
                                            <a href="<?= Yii::$app->homeUrl?>site/successful"> Stories
                                                <span class="arrow"></span>
                                            </a>
                                        </li>
                                        <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown ">
                                            <a href="<?= Yii::$app->homeUrl?>daily-bussiness/index"> Updates
                                                <span class="arrow"></span>
                                            </a>
                                        </li>
										<?php if(Yii::$app->user->id == 1){ ?>
										<li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown ">
                                                      <a href="">
                                                 Settings
                                                <span class="arrow"></span>
                                            </a>
                                                  <ul class="dropdown-menu pull-left">
                                                  <li aria-haspopup="true" class="">
                                                    <a href="<?= Yii::$app->homeUrl?>questions/index" class="nav-link nav-toggle ">
                                                        <i class=""></i> Create Questions
                                                        <span class="arrow"></span>
                                                    </a>
                                                </li>
                                                <li aria-haspopup="true" class="">
                                                    <a href="<?= Yii::$app->homeUrl?>sub-question-type/index" class="nav-link nav-toggle ">
                                                        <i class=""></i> Create Question Sub-Catagory
                                                        <span class="arrow"></span>
                                                    </a>
                                                </li>

                                                <li aria-haspopup="true" class="">
                                                    <a href="<?= Yii::$app->homeUrl?>question-type/index" class="nav-link nav-toggle ">
                                                        <i class=""></i> Create Question Catagory
                                                        <span class="arrow"></span>
                                                    </a>
                                                </li>

                                                <li aria-haspopup="true" class="">
                                                    <a href="<?= Yii::$app->homeUrl?>answer-options/index" class="nav-link nav-toggle ">
                                                        <i class=""></i> Create Answer Option
                                                        <span class="arrow"></span>
                                                    </a>
                                                </li>
                                                <li aria-haspopup="true" class="">
                                                    <a href="<?= Yii::$app->homeUrl?>post/index" class="nav-link nav-toggle ">
                                                        <i class=""></i> Create Story
                                                        <span class="arrow"></span>
                                                    </a>
                                                </li>
                                                <li aria-haspopup="true" class="">
                                                    <a href="<?= Yii::$app->homeUrl?>post-category/index" class="nav-link nav-toggle ">
                                                        <i class=""></i> Create Story Category
                                                        <span class="arrow"></span>
                                                    </a>
                                                </li>
                                                <li aria-haspopup="true" class="">
                                                    <a href="<?= Yii::$app->homeUrl?>daily-city/index" class="nav-link nav-toggle ">
                                                        <i class=""></i> Create Daily City
                                                        <span class="arrow"></span>
                                                    </a>
                                                </li>
                                                <li aria-haspopup="true" class="">
                                                     <a href="<?= Yii::$app->homeUrl?>daily-bussiness-cat/index " class="nav-link nav-toggle ">
                                                          <i class=""></i> Create Daily b category
                                                        <span class="arrow"></span>
                                                    </a>
                                                </li>
                                                <li aria-haspopup="true" class="">
                                                    <a href="<?= Yii::$app->homeUrl?>daily-bussiness/index" class="nav-link nav-toggle ">
                                                        <i class=""></i> Create Daily Product
                                                        <span class="arrow"></span>
                                                    </a>
                                                </li>
												<li aria-haspopup="true" class="">
                                                    <a href="<?= Yii::$app->homeUrl?>bussiness-name/index" class="nav-link nav-toggle ">
                                                        <i class=""></i> Create Locator BUSSINESS
                                                        <span class="arrow"></span>
                                                    </a>
                                                </li>
                                                <li aria-haspopup="true" class="">
                                                    <a href="<?= Yii::$app->homeUrl?>bussiness-city/index"  class="nav-link nav-toggle ">
                                                        <i class=""></i> Create Locator City
                                                        <span class="arrow"></span>
                                                    </a>
                                                </li>
                                                <li aria-haspopup="true" class="">
                                                    <a href="<?= Yii::$app->homeUrl?>bussiness-city-child/index " class="nav-link nav-toggle ">
                                                        <i class=""></i> Create Locator ChildCity
                                                        <span class="arrow"></span>
                                                    </a>
                                                </li>


                                            </ul>
                                        </li>


                                            <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown ">
                                                <a href="#">
                                                    Reports
                                                    <span class="arrow"></span>
                                                </a>
                                                <ul class="dropdown-menu pull-left">

                                                    <li aria-haspopup="true" class="">
                                                        <a href="<?= Yii::$app->homeUrl?>site/userreport" class="nav-link nav-toggle ">
                                                            <i class=""></i> User Report
                                                            <span class="arrow"></span>
                                                        </a>
                                                    </li>
                                                    <li aria-haspopup="true" class="">
                                                        <a href="<?= Yii::$app->homeUrl?>resultreport/index" class="nav-link nav-toggle ">
                                                            <i class=""></i> All Test Report
                                                            <span class="arrow"></span>
                                                        </a>
                                                    </li>
                                                </ul>

                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <!-- END MEGA MENU -->
                            </div>
						</div>

                        <div class="col-md-2 col-sm-6 col-xs-6">
																	<!-- BEGIN TOP NAVIGATION MENU -->
                                <div class="top-menu">
                                    <ul class="nav navbar-nav pull-right">
                                        <?php
                                        if (Yii::$app->user->isGuest) {?>
                                            <a href="<?= Yii::$app->homeUrl?>site/login" class="btn btn-success">Sign Up</a>
                                            <a href="<?= Yii::$app->homeUrl?>site/login" class="btn btn-primary">Login</a>
                                     <?php   }else {
                                            ?>
                                        <!-- BEGIN USER LOGIN DROPDOWN -->
                                        <li class="dropdown dropdown-user dropdown-dark">
                                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                                <img alt="" class="img-circle" src="../assets/layouts/layout3/img/avatar9.jpg">
                                                <i class="icon-user"></i>
                                                <span class="username username-hide-mobile">
                                                    <?= Yii::$app->user->identity->username?>
                                                </span>
                                            </a>
                                            <ul class="dropdown-menu pull-left">
                                                <li aria-haspopup="true" class="">
                                                    <a href="<?= Yii::$app->homeUrl?>site/updateuser" class="nav-link nav-toggle ">
                                                        <i class=""></i> Update User
                                                        <span class="arrow"></span>
                                                    </a>
                                                </li>
                                                <li aria-haspopup="true" class="">
                                                    <a href="<?= Yii::$app->homeUrl?>site/changepass" class="nav-link nav-toggle ">
                                                        <i class=""></i> Change Password
                                                        <span class="arrow"></span>
                                                    </a>
                                                </li>
                                                <li aria-haspopup="true" class="">
                                                    <a href="<?= Yii::$app->homeUrl?>site/logout" class="nav-link nav-toggle ">
                                                        <i class=""></i> Logout
                                                        <span class="arrow"></span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>


                                        <?php } ?>
                                        <!-- END USER LOGIN DROPDOWN -->
                                        <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                                        <!-- END QUICK SIDEBAR TOGGLER -->
                                    </ul>
									<!-- BEGIN RESPONSIVE MENU TOGGLER -->
                                <a href="javascript:;" class="menu-toggler"></a>
                                <!-- END RESPONSIVE MENU TOGGLER -->
								
                                </div>
                                <!-- END TOP NAVIGATION MENU -->
														</div>
														
						
							</div> <!-- closer Row-->
							
														
			
					</div>
				</div>
	</header>
                               
                            </div>
                        </div>
                        <!-- END HEADER TOP -->
						
                        
                    </div>
                    <!-- END HEADER -->
                </div>
    </div>
	
	
	
	
	
	
	
    <div class="page-wrapper-row full-height">
                <!--<div class="page-wrapper-middle">-->
                    <!-- BEGIN CONTAINER -->
                    <div class="page-container">
                        <!-- BEGIN CONTENT -->
                        <div class="page-content-wrapper">
								  <?= $content ?>

                        </div>
                    </div>
               <!-- </div>-->
    </div>
	
	     <!-- New Footer -->
			<footer class="black-bg">
                <div class="widget-area pt-70 pb-30">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4 mb-30">
                                <div class="footer-widget">
                                    <img class="mb-30" src="img/logo/logo-white.png" alt="">
                                    <p>Sed ut perspiciatis unde omnis iste nerror sit voluptatem accusantium dolorem tium totam rem
                                        aperiam eaque ipsa quae ab illose
                                        inventore veritatis et quasi artecto beatae vitae dicta sunt the way.</p>
                                </div>
                            </div>
                            <div class="col-xl-2 col-lg-2 col-md-6 mb-30 d-lg-none d-xl-block">
                                <div class="footer-widget">
                                    <h3>Usefull links</h3>
                                    <ul class="footer-link">
                                        <li><a href="<?= Yii::$app->homeUrl?>site/login">Sign Up</a></li>
                                        <li><a href="<?= Yii::$app->homeUrl?>site/login">Login</a></li>
                                        <!--<li><a href="#">Create Post</a></li>
                                        <li><a href="#">Create Post Catagory</a></li>
                                        <li><a href="#">Create Business</a></li>-->
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xl-2 col-lg-2 col-md-6 mb-30 d-lg-none d-xl-block">
                                <div class="footer-widget">
                                    <h3>Site links</h3>
                                    <ul class="footer-link">
                                        <li><a href="<?= Yii::$app->homeUrl?>">Home</a></li>

                                        <li><a href="<?= Yii::$app->homeUrl?>site/bussiness">Business</a></li>
                                        <li><a href="<?= Yii::$app->homeUrl?>site/bussinessinfo">Business Info</a></li>
                                        <li><a href="<?= Yii::$app->homeUrl?>site/successful">Stories</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-4 mb-30">
                                <div class="footer-widget">
                                    <h3>Contact Us</h3>
                                    <ul class="footer-info">
                                        <li>40 Baria Sreet 133/2 Gujrat City, Pakistan</li>
                                        <li>Phone: +92 (321) - 616 6195 </li>
                                        <li>Email:iamshahzaib89@gmail.com, </li>
                                        <li>Web: interpenuire.com</li>
                                        <li>Open hours: 8.00-18.00 Mon-Fri</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="copyright-area pt-25 pb-25">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-12">
                                <div class="copyright-text text-center text-lg-left">
                                    <p>Copyright © 2018 ENTERPENURE. All Rights Reserved</p>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <div class="footer-menu text-center  text-lg-right">
                                    <nav>
                                        <ul>
                                            <li><a href="#">Terms &amp; Condition</a></li>
                                            <li><a href="#">Privacy Policy</a></li>
                                            <li><a href="#"> Contact Us</a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
								
		 
		 
		 <!-- New Footer --->
	

	
	
     
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
