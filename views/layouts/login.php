<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\LoginAsset;

LoginAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script src="<?= Yii::$app->homeUrl?>src/assets/global/plugins/jquery.min.js"></script>
</head>
<body style="background-color: #cccccc;"  class="login"  >
<?php $this->beginBody() ?>

<div class="logohead">
<div class="logo">
    <a href="#">
        <img src="<?php echo Yii::$app->homeUrl?>src/assets/images/Logo-Enterpenure1.png" alt=""> </a>
</div>
</div>



<div class="content">
    <?=$content?>
</div>




<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
