<?php
/**
 * Created by PhpStorm.
 * User: barshan-mce
 * Date: 10-Aug-18
 * Time: 3:21 PM
 */
?>

<style>
    .table-error{
        background: coral;
    }
</style>
<div class="site-test">
    <div class="page-content-inner">
        <div class="row">
            <div class="col-md-12">

                <div class="portlet light " id="form_wizard_1">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-red"></i>
                            <span class="caption-subject font-red bold uppercase"> WIZARD -
                                                                <span class="step-title"> Step 1 of 3 </span>
                                                            </span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <form class="form-horizontal" action="<?= Yii::$app->homeUrl?>site/submit" id="submit_form" method="POST">
                            <div class="form-wizard">
                                <div class="form-body">
                                    <ul class="nav nav-pills nav-justified steps">

                                        <li>
                                            <a href="#tab2" data-toggle="tab" class="step">
                                                <span class="number"> 1 </span>
                                                <span class="desc">
                                                <i class="fa fa-check"></i> Test </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#tab3" data-toggle="tab" class="step">
                                                <span class="number"> 2 </span>
                                                <span class="desc">
                                                <i class="fa fa-check"></i> Confirm </span>
                                            </a>
                                        </li>
                                    </ul>
                                    <div id="bar" class="progress progress-striped" role="progressbar">
                                        <div class="progress-bar progress-bar-success"> </div>
                                    </div>
                                    <div class="tab-content">
                                        <div class="alert alert-danger display-none">
                                            <button class="close" data-dismiss="alert"></button> You have some form errors. Please check below. </div>
                                        <div class="alert alert-success display-none">
                                            <button class="close" data-dismiss="alert"></button> Your form validation is successful! </div>
                                        <div class="tab-pane" id="tab2">
                                            <div class="test-form">
                                                <table class="table table-bordered table-responsive table-condensed text-center">
                                                    <thead>
                                                    <tr >
                                                        <th>Categories</th>
                                                        <th>&nbsp;Questions</th>
                                                        <?php $ansoption = \app\models\AnswerOptions::find()->all(); //echo '<pre>'; print_r($revtype);
                                                        foreach($ansoption as $ans){
                                                            ?>
                                                            <th ><?= $ans->options?></th>
                                                            <?php
                                                        }
                                                        ?>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php $type = \app\models\QuestionType::find()->all();
                                                    foreach($type as $k=>$v){
                                                        $question = \app\models\Questions::find()->where('q_types_id = '.$v->id)->orderBy(['rand()' => SORT_DESC])->limit(10)->all();
                                                        foreach($question as $d=>$p){
                                                            ?>
                                                            <input type="hidden" value="<?php echo $v->id?>" name="type[<?= $p->id ?>]">
                                                            <tr>
                                                                <?php if($question[$d]['q_types_id']!=$question[$d-1]['q_types_id'])
                                                                {
                                                                    ?>
                                                                    <td class="centercontent" rowspan="<?= count($question)?>"><?= $v->name ?></td>
                                                                    <?php
                                                                } ?>
                                                                <td><?= $p->question ?></td>
                                                                <input type="hidden" value="<?php echo $p->id?>" name="question[<?= $p->id ?>]">
                                                                <?php $ansoption = \app\models\AnswerOptions::find()->all(); //echo '<pre>'; print_r($revtype);
                                                                foreach($ansoption as $m){
                                                                    ?>
                                                                    <td>
                                                                        <div class="radio">
                                                                            <label><input type="radio" id='subcatradio_<?= $m->id ?>' value="<?= $m->id ?>" name="subcatradio[<?= $p->id ?>]"></label>
                                                                        </div>
                                                                    </td>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </tr>
                                                        <?php } ?>
                                                        <?php
                                                    }
                                                    ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab3">
                                            Confirm This
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <a href="javascript:;" class="btn default button-previous">
                                                <i class="fa fa-angle-left"></i> Back </a>
                                            <a href="javascript:;" class="btn btn-outline green button-next"> Continue
                                                <i class="fa fa-angle-right"></i>
                                            </a>
                                            <button type="submit" class="btn green button-submit"> Submit
                                                <i class="fa fa-check"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT INNER -->


