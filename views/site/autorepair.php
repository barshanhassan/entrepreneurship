<?php
/**
 * Created by PhpStorm.
 * User: barshan-mce
 * Date: 11-Oct-18
 * Time: 11:57 AM
 */

?>

<section class="mid-business">
			<div class="container">
					<div  class="text-center text-primary busi-mid">
				      <h1> Auto Repair </h1>
					</div>
			</div>
</section>


<div class="container">

		
		
			 <div class="col-md-9">
						<div class="text-center text-primary bold">
							<h2 class="center-align primary bold " style="padding-bottom: 20px">Just Follow These Steps</h2>
						</div>
						
						<div class="row pb-25 pt-25">
							<div class="col-md-3">
							 <img src="<?= Yii::$app->homeUrl?>/src/auto/business.jpg" alt="" width="100%">
							
							</div>
							<div class="col-md-9">
								<div class="inpage-mid-text">
								<h3 class="bold">1. Business Plan</h3>
								<p>The most important factor to start a business is to prepare a proper business plan. A good business plan explains all the minute details which is required to start a business. Whether selecting a location, or arrangement of finance, or hiring employees, each activity associated with the business requires planning. A business started without any plan is liable to fail. Therefore, the first step to start an auto repair business is to write down a business plan. If you are unable to write down the business plan, you can take assistance of any small business administrative.</p>
								</div>
							</div>	
						</div>
						
						<div class="row pb-25 pt-25">
							<div class="col-md-9">
								<div class="inpage-mid-text">
								<h3 class="bold">2. Experience</h3>
								<p>Most of the people, find it hard to start an auto-repairing business. If you have hands-on experience in this field, then it's an added advantage for you. However, if you do not have any knowledge, you can join any training course and get the certification. If you do not wish to indulge yourself in servicing and just want to be the owner, then you must hire experienced mechanics and technicians for your business. Hiring qualified and talented mechanics is very difficult task. The owner of the business must take full time and do proper verification before hiring any employee</p>
								</div>
							</div>	
							<div class="col-md-3">
							 <img src="<?= Yii::$app->homeUrl?>/src/auto/experiance.jpg" alt="" width="100%">
							
							</div>
						</div>
						
						<div class="row pb-25 pt-25">
							<div class="col-md-3">
							 <img src="<?= Yii::$app->homeUrl?>/src/auto/tools.jpg" alt="" width="100%">
							
							</div>
							<div class="col-md-9">
								<div class="inpage-mid-text">
								<h3 class="bold">3. Tools</h3>
								<p>A good auto repair businessman always aims to organize the garage which fits the business needs. A good auto repair shop possesses an organized room for the spare parts in which all the tools and supplies are arranged in orderly manner. The tools used in auto-repairing service can be cheap as well as expensive. The small tools such as wrenches, air tool can be bought at cheap price. But, tools like lift, wheel balancer, brake lathe are costly. If the requirement of the business is to buy additional tools in order to run the new business smoothly and effectively, then it should be purchased right away. But, budget should always be concerned. Wasting money on unnecessary and extra things is not a good idea for the business.</p>
								</div>
							</div>	
							
						</div>
						
						<div class="row pb-25 pt-25">
							
							<div class="col-md-9">
								<div class="inpage-mid-text">
								<h3 class="bold">4. Price</h3>
								<p>Set up the competitive rate for the services being offered at your auto repair shop. The rates must neither be too high that customers find the service expensive and hesitate to come to your shop. Nor, it must be too low that your business goes in the loss.</p>
								</div>
							</div>	
							<div class="col-md-3">
							 <img src="<?= Yii::$app->homeUrl?>/src/auto/ruppes.jpg" alt="" width="100%">
							
							</div>
							
						</div>
						
						<div class="row pb-25 pt-25">
							<div class="col-md-3">
							 <img src="<?= Yii::$app->homeUrl?>/src/auto/advertising.jpg" alt="" width="100%">
							
							</div>
							<div class="col-md-9">
								<div class="inpage-mid-text">
								<h3 class="bold">5. Advertisement</h3>
								<p>Publicity of your business is must. Use pamphlets, newspapers to advertize your business. The flyer must mention your business, name, address and contact number if possible. One can ask the local distributor to place the pamphlets in the daily newspaper. You can place a small advertisement in the classified section of the newspaper. Ask your friends and relatives to tell others about your shop and its services. Try to provide attractive offers to attract the customers. You will have to be creative to make your business popular. A proper assessment helps the businessman to know that profit is generated from which type of advertisement. Do a survey to know that most of the customers coming to your shop come from which area.</p>
								</div>
							</div>	
							
							
						</div>
						
						<div class="row pb-25 pt-25">
							
							<div class="col-md-9">
								<div class="inpage-mid-text">
								<h3 class="bold">6. Protection & Insurance</h3>
								<p>Protection is must for any business. A good business is the one who has made all the necessary arrangements to tackle any type of mishaps. Therefore try to get correct insurance for your which would protect you and your business completely.</p>
								</div>
							</div>	
							<div class="col-md-3">
							 <img src="<?= Yii::$app->homeUrl?>/src/auto/protection.jpg" alt="" width="100%">
							
							</div>
							
							
						</div>
						
						
						
						
						
						
			 </div>
			 
			 
			 
			 <!--sidebar-->
			  <div class="col-md-3">
								<div class="servide-list">
									<h3> How To Run Business</h3>
								
								<ul>
									<li><a href="<?= Yii::$app->homeUrl ?>/site/vegitables">Supply Vegetables </a> </li>
									<li><a href="<?= Yii::$app->homeUrl ?>/site/desiproducts">Desi Products</a> </li>
									<li><a href="<?= Yii::$app->homeUrl ?>/site/freshmilk">Fresh Milk Supplier</a> </li>
									<li><a href="<?= Yii::$app->homeUrl ?>/site/mobilegarage">Energy</a> </li>
									<li><a href="<?= Yii::$app->homeUrl ?>/site/autorepair">Auto Repair</a> </li>
									
								</ul>
							</div>
			  
			
			 </div>
			 <!--Close Sidebar-->
</div>			 







<div class="site-index">
    <div class="container-fluid">
        <div class="container">
            <div class="text-center text-primary bold">
                <h2 class="center-align primary bold " style="padding-bottom:20px"></h2>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="service_item">
                        <img src="<?= Yii::$app->homeUrl?>/src/vegitables/suggestion.jpg" alt="" width="575px" height="320px">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="service_item">
                        <h3 class="bold">Few Suggestions:</h3>
                        <p>
						<ul>
                           <li> Make an Android app of your website as most of the house wives are using smart phones now a day and so it will be easy for them to order as they will not have start your computer again and again.</li>
                           <li> Keep a English and Hindi website converter button at the top of the website, for those who don’t know the English names of many grocery items, like jeera etc.</li>
                           <li> Contact number in the top and facility to take order on phone, as many would like order that way.</li>
                           <li> Show different sizes of the packages in the page of the product. Like 250gm, 500gm and 1kg sugar.</li>
                           <li> In second phase you can also selling bakery items, fruits and orders from restaurants.</li>
                           <li> You can take delivery charges in the range of rs30 to rs80 on orders of below Rs250 ticket size.</li>
                        </ul>
						</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
.service_item ul li{margin: 7px 0px;}
</style>