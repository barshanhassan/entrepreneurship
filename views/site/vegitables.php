<?php
/**
 * Created by PhpStorm.
 * User: barshan-mce
 * Date: 09-Oct-18
 * Time: 2:24 PM
 */
$this->title = 'Vegitable Tables ';
?>

<section class="mid-business">
			<div class="container">
					<div  class="text-center text-primary busi-mid">
				      <h1> Supply Vegetables </h1>
					</div>
			
			
			</div>
	</section>
	
<div class="container">

		
		
			 <div class="col-md-9">
						<div class="text-center text-primary bold">
							<h2 class="center-align primary bold " style="padding-bottom: 20px">Just Follow These Steps</h2>
						</div>
						<div class="row pb-25 pt-25">
							<div class="col-md-3">
							 <img src="<?= Yii::$app->homeUrl?>/src/vegitables/vegetable1.jpg" alt="" width="100%">
							
							</div>
							<div class="col-md-9">
								<div class="inpage-mid-text">
								<h3 class="bold">1. Arrangement of Stock</h3>
								<p>Either you can have your stock or you can do a tie-up with a present store initially. Prepare a list of products (with their margins) you want to keep in the store and arrange accordingly.</p>
								</div>
							</div>	
						</div>
						
						<div class="row pb-25 pt-25">
							<div class="col-md-9">
								<div class="inpage-mid-text">
								<h3 class="bold">2. Development of website</h3>
								<p>Get your grocery website developed by any good professional Company who has experience in this field like Nationkart etc. Upload the photos and the rates of your products in the website. Showcase 2-3 good offers in your website.</p>
								</div>
							</div>	
							<div class="col-md-3">
							 <img src="<?= Yii::$app->homeUrl?>/src/vegitables/web.jpg" alt="" width="100%">
							
							</div>
						</div>
						
						<div class="row pb-25 pt-25">
							<div class="col-md-3">
							 <img src="<?= Yii::$app->homeUrl?>/src/vegitables/marketing.jpg" alt="" width="100%">
							
							</div>
							<div class="col-md-9">
								<div class="inpage-mid-text">
								<h3 class="bold">3. Marketing</h3>
								<p>You can market your service in local newspaper, through pamphlets, SMSs, hoardings, door to door meeting etc. And location based Online marketing is also preferable.</p>
								</div>
							</div>	
							
						</div>
						
						<div class="row pb-25 pt-25">
							<div class="col-md-9">
								<div class="inpage-mid-text">
								<h3 class="bold">4. Development Local Delivery System</h3>
								<p>You will have to decide the range of your delivery. For example 5km, 10km radius or whole of the city. Then arrange a 2 or 3 delivery boys with bikes.</p>
								</div>
							</div>	
							<div class="col-md-3">
							 <img src="<?= Yii::$app->homeUrl?>/src/vegitables/delivery.jpg" alt="" width="100%">
							
							</div>
							
						</div>
						
						<div class="row pb-25 pt-25">
							<div class="col-md-3">
							 <img src="<?= Yii::$app->homeUrl?>/src/vegitables/cash.jpg" alt="" width="100%">
							
							</div>
							<div class="col-md-9">
								<div class="inpage-mid-text">
								<h3 class="bold">5. Payment Method Local Delivery System</h3>
								<p>You can give COD (Cash on Delivery) facility (this is preferable) to your customers this would be easy as your own staff will be delivering the goods to the customers house, so you will receive the payment same day unlike in other e-commerce businesses where the courier companies collect money on your behalf and then give you after a week after deducting COD charges. If you want you can even get the payment through debit card or net banking, but thru this payment method the payment gateway company will deduct their charges and then give you the amount after 5 days. So COD is preferable for Online Grocery Stores.</p>
								</div>
							</div>	
							
							
						</div>
						
						
						
						
						
						
						
						<div class="inpage-mid-text">
							<h3 class="bold"></h3>
							<p></p>
						</div>
						
						
		
			 </div>
			 
			 
			 <!--sidebar-->
			  <div class="col-md-3">
								<div class="servide-list">
									<h3> How To Run Business</h3>
								
								<ul>
									<li><a href="<?= Yii::$app->homeUrl ?>/site/vegitables">Supply Vegetables </a> </li>
									<li><a href="<?= Yii::$app->homeUrl ?>/site/desiproducts">Desi Products</a> </li>
									<li><a href="<?= Yii::$app->homeUrl ?>/site/freshmilk">Fresh Milk Supplier</a> </li>
									<li><a href="<?= Yii::$app->homeUrl ?>/site/mobilegarage">Energy</a> </li>
									<li><a href="<?= Yii::$app->homeUrl ?>/site/autorepair">Auto Repair</a> </li>
									
								</ul>
							</div>
			  
			
			 </div>
			 <!--Close Sidebar-->
	
	
	
	</div>	
	
	
	<div class="container">
   
    <div class="row">
        <div class="col-md-6">
            <div class="service_item">
                <img src="<?= Yii::$app->homeUrl?>/src/vegitables/suggestion.jpg" alt="" width="575px" height="320px">
            </div>
        </div>
		
        <div class="col-md-6">
            <div class="service_item">
                <h3 class="bold">Few Suggestions:</h3>
                <p>
				<ul>
                   <li> 1. Make an Android app of your website as most of the house wives are using smart phones now a day and so it will be easy for them to order as they will not have start your computer again and again.</li>
                   <li>2. Keep a English and Hindi website converter button at the top of the website, for those who don’t know the English names of many grocery items, like jeera etc.</li>
                   <li> 3. Contact number in the top and facility to take order on phone, as many would like order that way.</li>
                   <li> 4. Show different sizes of the packages in the page of the product. Like 250gm, 500gm and 1kg sugar.</li>
                   <li> 5. In second phase you can also selling bakery items, fruits and orders from restaurants.</li>
                   <li> 6. You can take delivery charges in the range of rs30 to rs80 on orders of below Rs250 ticket size</li>
                </ul>
				</p>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
.service_item ul li{margin: 7px 0px;}
</style>
	



