<?php
/**
 * Created by PhpStorm.
 * User: barshan-mce
 * Date: 09-Oct-18
 * Time: 4:43 PM
 */

?>

<section class="mid-business">
			<div class="container">
					<div  class="text-center text-primary busi-mid">
				      <h1> Desi Produts </h1>
					</div>
			</div>
</section>

<div class="container">

		
		
			 <div class="col-md-9">
						<div class="text-center text-primary bold">
							<h2 class="center-align primary bold " style="padding-bottom: 20px">Just Follow These Steps</h2>
						</div>
						
						<div class="row pb-25 pt-25">
							<div class="col-md-3">
							 <img src="<?= Yii::$app->homeUrl?>/src/desi/desi1.jpg" alt="" width="100%">
							
							</div>
							<div class="col-md-9">
								<div class="inpage-mid-text">
								<h3 class="bold">1. Shop</h3>
								<p>First you need a good shop at good area. Decorate shop according to products. Because if arrangement of all project good .So gave good impression.</p>
								</div>
							</div>	
						</div>
						
						<div class="row pb-25 pt-25">
							<div class="col-md-9">
								<div class="inpage-mid-text">
								<h3 class="bold">2. Product Arrangement</h3>
								<p>Because arrangement of product is tough Desi product contain Ghee ,eggs ,hens and other things. You should need large setup to collect all products. you should have a good team to collect all these things.</p>
								</div>
							</div>	
							<div class="col-md-3">
							 <img src="<?= Yii::$app->homeUrl?>/src/desi/pro-arrange.jpg" alt="" width="100%">
							
							</div>
						</div>
						
						<div class="row pb-25 pt-25">
							<div class="col-md-3">
							 <img src="<?= Yii::$app->homeUrl?>/src/desi/vehicle.jpg" alt="" width="100%">
							
							</div>
							<div class="col-md-9">
								<div class="inpage-mid-text">
								<h3 class="bold">3. Vehcile</h3>
								<p>A good vehicle, like (moter cycle) or other good loder vehcile. Purpose If you want to start desi product. All these things collect from different places. So you must have a good vehicle.</p>
								</div>
							</div>	
							
						</div>
						
						<div class="row pb-25 pt-25">
							<div class="col-md-9">
								<div class="inpage-mid-text">
								<h3 class="bold">4. Money</h3>
								<p>A business owner should have enough finance to pay all the business as well as personal bills. Before starting the business, a person should try to arrange the money from family members, friends and relatives. One can take loans from the bank providing the necessary documents. Do not waste the money on unwanted things. A good businessman never lets the situation go off the head and is always prepared for the worst situation. Therefore, financial management is very important to run the business efficiently.</p>
								</div>
							</div>	
							<div class="col-md-3">
							 <img src="<?= Yii::$app->homeUrl?>/src/desi/money.jpg" alt="" width="100%">
							
							</div>
							
						</div>
						
						<div class="row pb-25 pt-25">
							<div class="col-md-3">
							 <img src="<?= Yii::$app->homeUrl?>/src/desi/price.jpg" alt="" width="100%">
							
							</div>
							<div class="col-md-9">
								<div class="inpage-mid-text">
								<h3 class="bold">5. Price</h3>
								<p>Rates must be according to market.</p>
								</div>
							</div>	
							
							
						</div>
						
						<div class="row pb-25 pt-25">
							
							<div class="col-md-9">
								<div class="inpage-mid-text">
								<h3 class="bold">6. Arrangement Of Expert</h3>
								<p>Have two or three experts’ people. they should know about every thing.</p>
								</div>
							</div>	
							<div class="col-md-3">
							 <img src="<?= Yii::$app->homeUrl?>/src/desi/expert.jpg" alt="" width="100%">
							
							</div>
							
							
						</div>
						
						
						
						
						
						
						
						
						
						
						
						
		
			 </div>
			 
			 
			 <!--sidebar-->
			  <div class="col-md-3">
								<div class="servide-list">
									<h3> How To Run Business</h3>
								
								<ul>
									<li><a href="<?= Yii::$app->homeUrl ?>/site/vegitables">Supply Vegetables </a> </li>
									<li><a href="<?= Yii::$app->homeUrl ?>/site/desiproducts">Desi Products</a> </li>
									<li><a href="<?= Yii::$app->homeUrl ?>/site/freshmilk">Fresh Milk Supplier</a> </li>
									<li><a href="<?= Yii::$app->homeUrl ?>/site/mobilegarage">Energy</a> </li>
									<li><a href="<?= Yii::$app->homeUrl ?>/site/autorepair">Auto Repair</a> </li>
									
								</ul>
							</div>
			  
			
			 </div>
			 <!--Close Sidebar-->
	
	
	
	</div>	



        <div class="container">
       
            <div class="row">
                <div class="col-md-6">
                    <div class="service_item">
                        <img src="<?= Yii::$app->homeUrl?>/src/vegitables/suggestion.jpg" alt="" width="575px" height="320px">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="service_item">
                        <h3 class="bold">Few Suggestions:</h3>
                        <p>
						<ul>
                            <li>1. Make an Android app of your website as most of the house wives are using smart phones now a day and so it will be easy for them to order as they will not have start your computer again and again.</li>
                            <li>2. Keep a English and Hindi website converter button at the top of the website, for those who don’t know the English names of many grocery items, like jeera etc.</li>
                            <li>3. Contact number in the top and facility to take order on phone, as many would like order that way.</li>
                            <li>4. Show different sizes of the packages in the page of the product. Like 250gm, 500gm and 1kg sugar.</li>
                            <li>5. In second phase you can also selling bakery items, fruits and orders from restaurants.</li>
                            <li>6. You can take delivery charges in the range of rs30 to rs80 on orders of below Rs250 ticket size.</li>
                        </ul>
						</p>
                    </div>
                </div>
            </div>
        </div>
 


<style type="text/css">
.service_item ul li{margin: 7px 0px;}
    .numberCircle {
        height: 100px;
        width: 100px;
        line-height:100px;
        text-align: center;
        border-radius: 50px;
        background: #1ACAC0;
        color:white;
        margin-left:auto;
        margin-right:auto;
    }

</style>