<?php
/**
 * Created by PhpStorm.
 * User: barshan-mce
 * Date: 10-Aug-18
 * Time: 3:21 PM
 */
?>

<style>
    .table-error{
        background: coral;
    }
</style>
<div class="site-test">
    <div class="page-content-inner">
        <div class="row">
            <div class="col-md-12">

                <div class="portlet light " id="form_wizard_1">

                    <div class="portlet-body form">
                        <form class="form-horizontal" action="<?= Yii::$app->homeUrl?>site/submitupdateuser" id="submit_form" method="POST">
                            <div class="form-wizard">
                                <div class="form-body">
                                    <ul class="nav nav-pills nav-justified steps">
                                        <?php
                                        $user_id = Yii::$app->user->id;

                                            $query =\app\models\User::find()->where('id = '.$user_id)->all();
                                            foreach ($query as $q) {

                                        ?>

                                         <li>
                                             <a href="#tab1" data-toggle="tab" class="step">
                                                 <span class="number"> 1 </span>
                                                 <span class="desc">
                                                     <i class="fa fa-check"></i> Profile Setup </span>
                                             </a>
                                         </li>
                                        <li>
                                            <a href="#tab3" data-toggle="tab" class="step">
                                                <span class="number"> 2 </span>
                                                <span class="desc">
                                                <i class="fa fa-check"></i> Confirm </span>
                                            </a>
                                        </li>
                                    </ul>
                                    <div id="bar" class="progress progress-striped" role="progressbar">
                                        <div class="progress-bar progress-bar-success"> </div>
                                    </div>
                                    <div class="tab-content">
                                        <div class="alert alert-danger display-none">
                                            <button class="close" data-dismiss="alert"></button> You have some form errors. Please check below. </div>
                                        <div class="alert alert-success display-none">
                                            <button class="close" data-dismiss="alert"></button> Your form validation is successful! </div>
                                        <div class="tab-pane" id="tab1">

                                        <input type="hidden" name="_csrf" value="<?/*=Yii::$app->request->getCsrfToken()*/?>" />

                                        <h3 class="block">Provide your profile details</h3>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Phone Number
                                                <span > * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="phone" value = "<?= $q->phone ?>" />
                                                <span class="help-block"> Provide your phone number </span>
                                            </div>

                                        </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Email
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="email" value="<?=$q->email?>"/>
                                                    <span class="help-block"> Provide your Email </span>
                                                </div>

                                            </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Age
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="age" value="<?=$q->age?>" />
                                                <span class="help-block"> Provide your Age Here </span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Gender
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <div class="radio-list">
                                                    <label>
                                                        <input type="radio" name="gender" value="M" data-title="Male" <?php if($q->gender == 'M') echo 'checked' ?>/> Male </label>
                                                    <label>
                                                        <input type="radio" name="gender" value="F" data-title="Female" <?php if($q->gender == 'F') echo 'checked' ?>/> Female </label>
                                                </div>
                                                <div id="form_gender_error"> </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Address
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="address" value = "<?=$q->addres?>" />
                                                <span class="help-block"> Provide your street address </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">City/Town
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="city" value = "<?=$q->city?>"/>
                                                <span class="help-block"> Provide your city or town </span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Qualification
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="qualification" value="<?=$q->qualification?>"/>
                                                <span class="help-block"> Provide your Qulafication </span>
                                            </div>
                                        </div>
                                        </div>
                                    <?php }
                                         ?>
                                        <div class="tab-pane" id="tab3">
                                            Confirm This
                                        </div>

                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <a href="javascript:;" class="btn default button-previous">
                                                <i class="fa fa-angle-left"></i> Back </a>
                                            <a href="javascript:;" class="btn btn-outline green button-next"> Continue
                                                <i class="fa fa-angle-right"></i>
                                            </a>
                                            <button type="submit" class="btn green button-submit"> Submit
                                                <i class="fa fa-check"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT INNER -->


