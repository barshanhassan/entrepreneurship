<?php

/* @var $this yii\web\View */

$this->title = 'Enterpenure ';
?>

<!--<div >
    <img src="<?/*= Yii::$app->homeUrl*/?>slider_2.jpg" width="1225px" height="400px" ">
</div>-->
<script type="text/javascript" src="<?= Yii::$app->homeUrl ?>js/tiny/js/app.js"></script>

<link rel="stylesheet" href="<?= Yii::$app->homeUrl ?>js/tiny/css/style.css">



<div id="slider-container">
    <img src="<?= Yii::$app->homeUrl ?>js/tiny/img/left-arrow.png" id="prev">
    <ul id="slider">
        <li class="slide">
            <div class="slide-copy">
                <p>The price of success is hard work</p>
            </div>
            <img src="<?= Yii::$app->homeUrl ?>src/slider_pics/slider-1.jpg" alt="">
        </li>
        <li class="slide">
            <div class="slide-copy">
                <!--<p>“Watch, listen, and learn.</p>-->
            </div>
            <img src="<?= Yii::$app->homeUrl ?>src/slider_pics/slider-2.jpg" alt="">
        </li>
        <li class="slide">
            <div class="slide-copy">

                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
            </div>
            <img src="<?= Yii::$app->homeUrl ?>src/slider_pics/slider-3.jpg" alt="">
        </li>
        <li class="slide">
            <div class="slide-copy">

                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
            </div>
            <img src="<?= Yii::$app->homeUrl ?>src/slider_pics/slider-4.jpg" alt="">
        </li>
    </ul>
    <img src="<?= Yii::$app->homeUrl ?>js/tiny/img/right-arrow.png" id="next">
</div>
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36251023-1']);
    _gaq.push(['_setDomainName', 'jqueryscript.net']);
    _gaq.push(['_trackPageview']);
    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>
<div class="container">
			<div  class="text-center text-primary head1">
				     <h2>What is Entrepreneurship!</h2>
					
					<p>Entrepreneurship is the process of designing, launching and running a new business, which is often initially a small business. <br>
					The people who create these businesses are called entrepreneurs</p>
			</div>
			



</div><!--close Container-->

<section class="mid">
<div class="container">
<div class="row">
        <div class="col-md-6">
            <div class="service_item">
                <img src="<?= Yii::$app->homeUrl?>/images/20170731101842-.jpg" alt="" width="100%">
            </div>
        </div>
		
		<div class="col-md-6">
			<div class="mid-text">
                <h3 class="bold">Enterpenure</h3>
                <p>The critical ingredient is getting off your but and doing something. It's as simple as that. A lot of people have ideas, but there are few who decide to do something about them now. Not tomorrow. Not next week. But today. The true entrepreneur is a doer, not a dreamer." -Nolan Bushnell, entrepreneur. </p>
            </div>
		</div>

		
		
		
		</div><!--row-->
		</div><!--container-->
</section>

<div class="container">
			<div  class="text-center text-primary head1">
				     <h2>News & Trends</h2>
					
					<p>Entrepreneurship is the process of designing, launching and running a new business, which is often initially a small business. <br>
					The people who create these businesses are called entrepreneurs</p>
			</div>
			
			<div class="row">
        <div class="col-md-4">
            <div class="service_item">

                <img src="<?= Yii::$app->homeUrl?>/images/20180915125115-woman-1446557-1920.jpg" alt="">

                <a href="#"><h3>It's Google v/s Alibaba in the Cloud Computing Gameplay in the Asia Pacific</h3></a>
                <p>While the Chinese tech giant Alibaba will be launching its data centers in Singapore and Japan, Google already counts the likes of AirAsia, Go-Jek, Mahindra and Mizuho Bank as clients </p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="service_item">
                <img src="<?= Yii::$app->homeUrl?>/images/20180915063108-hand-3308188.jpg" alt="">
                <a href="#"><h3>By 2028 AI Could Take Away 28 Million Jobs in ASEAN Countries</h3></a>
                <p>The overall job landscape will look very different in 2028 because where the jobs are created is different from where the jobs are displaced </p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="service_item">
                <img src="<?= Yii::$app->homeUrl?>/images/20180914145706-GettyImages-1032946918.jpg" alt="">
                <a href="#"><h3>Jeff Bezos Did What?! The Week In Entrepreneur News Quiz</h3></a>
                <p>How plugged in are you? Find out! </p>
            </div>
        </div>
    </div>
			
			



</div><!--close Container-->

