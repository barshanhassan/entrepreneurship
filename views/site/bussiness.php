<?php
/**
 * Created by PhpStorm.
 * User: barshan-mce
 * Date: 24-Sep-18
 * Time: 11:35 AM
 */


?>

<!DOCTYPE html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <title>Google Maps Multiple Markers</title>
   <!-- <script src="http://maps.google.com/maps/api/js?sensor=false"type="text/javascript"></script> -->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAGYtropNyECkVd8whURbBzBxdmL8QfvVE" type="text/javascript"></script>

</head>
    <body>
	<section class="mid-business">
        <div class="container">
            <div  class="text-center text-primary busi-mid">
              <h1> Entrepreneurship </h1>
            </div>
        </div>
	</section>
        <div class="container-fluid">
		   <div class="center-block search-area">
               <form method="post" >
                   <div class="col-md-6">
                        <select style="width: 350px;" class="serach-selec" name="bussiness">
                            <?php if(isset($_POST['city']) && isset($_POST['bussiness'])) {
                                $selected = 'selected';
                            }
                                ?>

                            <option value="" class="bold">Select Product Here</option>
                            <?php
                             $bname = \app\models\BussinessName::find()->all();
                              foreach ($bname as $b) {?>
                            <option value="<?= $b['id']?>" <?php if($_POST['bussiness']== $b['id']){echo $selected;}?>><?= $b['bussiness_name']?> </option>
                            <?php } ?>
                        </select>
                   </div>
                
			       <div class="col-md-6">
                    <select style="width: 350px;" class="serach-selec" name="city">
                        <option value="" class="bold">Select City Here</option>
                    <?php
                    $cname = \app\models\BussinessCity::find()->all();
                    foreach ($cname as $c) {?>
                        <option value="<?= $c['id']?>" <?php if($_POST['city']== $c['id']){echo $selected;}?>><?= $c['bussiness_name'] ?></option>
                    <?php } ?>
                    </select>
                   </div>
                   <input class= "btn-search" type="submit" name="submit" value="Search">
               </form>
           </div>
        </div>

    <div id="map" style="width: 100%; height: 550px;"></div>
    <script type="text/javascript">
        document.getElementById('city').value = "<?php echo $_GET['location'];?>";
    </script>
    <?php

    if(isset($_POST['city']) && isset($_POST['bussiness'])){

        $bname = $_POST['bussiness'];
        $city = $_POST['city'];

        $getCityLatLang = \app\models\BussinessCity::find()->where('id='.$city)->all();
        foreach ($getCityLatLang as $cityLtLg){
            $cityLt= $cityLtLg['latitude'];
            $cityLg = $cityLtLg['langitutde'];
        }

        /*echo '<pre>';
            echo print_r($getCityLatLang);
            echo '</pre>';
            exit;*/
        $getLatLog = \app\models\BussinessCityChild::find()->where('bussiness_name_id='.$bname)->andWhere('bussiness_city_id='. $city)->all();

			$location = array();
			$i = 1;
            foreach ($getLatLog as $ltlg){
                $loc[]  = $ltlg['city_child_name'];
                $loc[]  = $ltlg['latitude'];
                $loc[] = $ltlg['longitude'];
                $loc[] = $i;
                $i++;
                $location[] = $loc;
                unset ($loc);
            }

	$resp = json_encode($location);
?>
        <script type="text/javascript">

        var locations = <?= $resp ?>;
           // locations.push(name, latitude, longitude,1);
        </script>
<?php }
else{
        $cityLt = 30.957305;
        $cityLg = 70.073002;
}?>
    <script type="text/javascript">
        var cityLt = '<?= $cityLt ?>';
        var cityLg = '<?= $cityLg ?>';

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 13,
            center: new google.maps.LatLng(cityLt, cityLg),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
    
        var infowindow = new google.maps.InfoWindow();
        var marker, i;
        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map
            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }
    </script>
    </body>
</html>