<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
.checkbox, .form-horizontal .checkbox {
    padding: 14px;
}
.btn.green:not(.btn-outline) {
    color: #FFF;
    background-color: #1f86c8;
    border-color: #1f86c8;
}
.font-green {
    color: #1f86c8!important;
}
</style>
<div class="site-login">
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}{input}{error}",
            'labelOptions' => ['class' => 'control-label visible-ie8 visible-ie9'],
        ],
    ]); ?>
    <h3 class="form-title font-green"><?= Html::encode($this->title) ?></h3>
        <?= $form->field($model, 'username')->textInput(['autofocus' => true ,'placeholder' => "Username"]) ?>
        <?= $form->field($model, 'password')->passwordInput(['placeholder' => "Password"]) ?>
<div class="col-sm-6">
<div class="form-group field-loginform-rememberme">

<div class="checkbox">
<label for="loginform-rememberme">
<input type="hidden" name="LoginForm[rememberMe]" value="0">
<input type="checkbox" id="loginform-rememberme" name="LoginForm[rememberMe]" value="1" checked="">
Remember Me
</label>
</div>


</div>
</div>
<div class="col-sm-6">
<div class="checkbox">
<?= Html::checkbox('reveal-password', false, ['id' => 'reveal-password']) ?> <?= Html::label('Show password', 'reveal-password') ?>
</div>
</div>

    <div class="form-actions">
                <?= Html::submitButton('Login', ['class' => 'btn green uppercase', 'name' => 'login-button']) ?>
        </div>
		
    <div class="create-account">
        <p>
            <a href="javascript:;" id="register-btn" class="uppercase">Sign up</a>
        </p>
    </div>

    <?php ActiveForm::end(); ?>
    <?php $form = ActiveForm::begin([
        'id' => 'register-form',
        'layout' => 'horizontal',
        'options'=> [
                //'class'=>'hidden',
                'id'=>'register-form',
        ],
        'fieldConfig' => [
            'template' => "{label}{input}{error}",
            'labelOptions' => ['class' => 'control-label visible-ie8 visible-ie9'],
        ],

    ]); ?>

    <h3 class="font-green">Sign Up</h3>


     <?= $form->field($model2,'name')->textInput(['autofocus' => true ,'placeholder' => "Enter Full Name"]) ?>


    <?= $form->field($model2, 'email', ['enableAjaxValidation' => true])->textInput(['autofocus' => true ,'placeholder' => "Enter Email"]) ?>

    <?= $form->field($model2, 'username', ['enableAjaxValidation' => true])->textInput(['autofocus' => true ,'placeholder' => "Enter UserName"]) ?>


    <?= $form->field($model2, 'password')->passwordInput(['placeholder' => "Password"]) ?>

    <?= $form->field($model2, 'repeat_password')->passwordInput(['placeholder' => " Re-type Password"]) ?>

    <div class="form-actions">
        <?= Html::submitButton('Back', ['id'=>'register-back-btn','class' => 'btn green btn-outline', 'name' => 'back-button']) ?>
        <?= Html::submitButton('Sign Up', ['id'=>'register-submit-btn','class' => 'btn btn-success uppercase pull-right', 'name' => 'signup-button']) ?>
    </div>

   <?php ActiveForm::end(); ?>

</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#register-form').hide();
        $("#register-btn").click(function(e) {
            $("#login-form").hide();
            $("#register-form").show();
            e.preventDefault();
            //$("#productionForm").show();
        });
        $("#register-back-btn").click(function(e) {
            //jQuery("#register-back-btn").click(function() {
            $("#login-form").show();
            $("#register-form").hide();
            e.preventDefault();
        });
    });
</script>

<?php
$this->registerJs("jQuery('#reveal-password').change(function(){jQuery('#loginform-password').attr('type',this.checked?'text':'password');})");
?>
