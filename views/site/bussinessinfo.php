<?php
/**
 * Created by PhpStorm.
 * User: barshan-mce
 * Date: 29-Sep-18
 * Time: 3:28 PM
 */

?>

<div id="slider-container">
    <img src="<?= Yii::$app->homeUrl ?>js/tiny/img/left-arrow.png" id="prev">
    <ul id="slider">
        <li class="slide">
            <div class="slide-copy">
                <p>The price of success is hard work</p>
            </div>
            <img src="<?= Yii::$app->homeUrl ?>src/slider_pics/slider-1.jpg" alt="">
        </li>
        <li class="slide">
            <div class="slide-copy">
                <!--<p>“Watch, listen, and learn.</p>-->
            </div>
            <img src="<?= Yii::$app->homeUrl ?>src/slider_pics/slider-2.jpg" alt="">
        </li>
        <li class="slide">
            <div class="slide-copy">

                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
            </div>
            <img src="<?= Yii::$app->homeUrl ?>src/slider_pics/slider-3.jpg" alt="">
        </li>
        <li class="slide">
            <div class="slide-copy">

                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
            </div>
            <img src="<?= Yii::$app->homeUrl ?>src/slider_pics/slider-4.jpg" alt="">
        </li>
    </ul>
    <img src="<?= Yii::$app->homeUrl ?>js/tiny/img/right-arrow.png" id="next">
</div>


		
	
	<div class="container">
			 <div class="col-md-9">
						<div class="inpage-mid-text">
							<h3 class="bold">Sole Proprietorship</h3>
							<p>A sole proprietorship is a business owned by only one person. It is easy to set-up and is 
							the least costly among all forms of ownership. The owner faces unlimited liability meaning, 
							the creditors of the business may go after the personal assets of the owner if the business 
							cannot pay them. The sole proprietorship form is usually adopted by small business entities.</p>
						</div>
						
						<div class="inpage-mid-text">
							<h3 class="bold">Partnership</h3>
							<p>A partnership is a business owned by two or more persons who contribute resources into the 
							entity. The partners divide the profits of the business among themselves. In general 
							partnerships, all partners have unlimited liability. In limited partnerships, creditors cannot 
							go after the personal assets of the limited partners.</p>
						</div>
						
						<div class="inpage-mid-text">
							<h3 class="bold">Corporation</h3>
							<p>A corporation is a business organization that has a separate legal personality from its 
							owners. Ownership in a stock corporation is represented by shares of stock. The owners 
							(stockholders) enjoy limited liability but have limited involvement in the company's operations.
							The board of directors, an elected group from the stockholders, controls the activities of the 
							corporation. In addition to those basic forms of business ownership, these are some other types 
							of organizations that are common today:</p>
						</div>
		
			 </div>
			 
			 
			 <!--sidebar-->
			  <div class="col-md-3">
								<div class="servide-list">
									<h3> How To Run Business</h3>
								
								<ul>
									<li><a href="<?= Yii::$app->homeUrl ?>/site/vegitables">Supply Vegetables </a> </li>
									<li><a href="<?= Yii::$app->homeUrl ?>/site/desiproducts">Desi Products</a> </li>
									<li><a href="<?= Yii::$app->homeUrl ?>/site/freshmilk">Fresh Milk Supplier</a> </li>
									<li><a href="<?= Yii::$app->homeUrl ?>/site/mobilegarage">Energy</a> </li>
									<li><a href="<?= Yii::$app->homeUrl ?>/site/autorepair">Auto Repair</a> </li>
									
								</ul>
							</div>
			  
			
			 </div>
			 <!--Close Sidebar-->
	
	
	
	</div>



<script type="text/javascript" src="<?= Yii::$app->homeUrl ?>js/tiny/js/app.js"></script>

<link rel="stylesheet" href="<?= Yii::$app->homeUrl ?>js/tiny/css/style.css">









<div class="container">
    <div class="text-center text-primary head2">
        <h2>Bussiness</h2>
    </div>
	
	<div class="col-md-4">
            <div class="service_item">
                <img src="<?= Yii::$app->homeUrl?>service.jpg" alt="">
                <a href="#"><h3>Service Business</h3></a>
                <p>A service type of business provides intangible products (products with no physical form). Service type firms offer professional skills, expertise, advice, and other similar products. Examples of service businesses are: salons, repair shops, schools, banks, accounting firms, and law firms.</p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="service_item">
                <img src="<?= Yii::$app->homeUrl?>march2.jpg" alt="" >
                <a href="#"><h3>Merchandising Business</h3></a>
                <p>This type of business buys products at wholesale price and sells the same at retail price. They are known as &quot;buy and sell&quot; businesses. They make profit by selling the products at prices higher than their purchase costs.
                A merchandising business sells a product without changing its form. Examples are: grocery stores, convenience stores, distributors, and other resellers.</p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="service_item">
                <img src="<?= Yii::$app->homeUrl?>manufecture.jpg" alt="" >
                <a href="#"><h3>Manufacturing  Business</h3></a>
                <p>Unlike a merchandising business, a manufacturing business buys products with the intention of using them as materials in making a new product. Thus, there is a transformation of the products purchased.
                A manufacturing business combines <em>raw materials, labor, and factory overhead</em> in its production process. The manufactured goods will then be sold to customers.</p>
            </div>
        </div>
	
	
	
    
</div>



<div class="container">
		<div class="serv ">
			<h3 class="text-primary">Types of Business Structures </h3>
			</div>
			
						<div class="inpage-mid-text">
							<h3 class="bold">Sole Proprietorship</h3>
							<p>A Sole Proprietorship is one individual or married couple in business alone. Sole proprietorships are the most common form of business structure. This type of business is simple to form and operate, and may enjoy greater flexibility of management, fewer legal controls, and fewer taxes. However, the business owner is personally liable for all debts incurred by the business.</p>
						</div>
						
						<div class="inpage-mid-text">
							<h3 class="bold">General Partnership</h3>
							<p>A General Partnership is composed of 2 or more persons (usually not a married couple) who agree to contribute money, labor, or skill to a business. Each partner shares the profits, losses, and management of the business, and each partner is personally and equally liable for debts of the partnership. Formal terms of the partnership are usually contained in a written partnership agreement.</p>
						</div>
						
						<div class="inpage-mid-text">
							<h3 class="bold">Limited Partnership</h3>
							<p>A Limited Partnership is composed of one or more general partners and one or more limited partners. The general partners manage the business and share fully in its profits and losses. Limited partners share in the profits of the business, but their losses are limited to the extent of their investment. Limited partners are usually not involved in the day-to-day operations of the business. Filing with the Washington Secretary of State is required.</p>
						</div>
						
						<div class="inpage-mid-text">
							<h3 class="bold">Limited Liability Partnership (LLP)</h3>
							<p>A Limited Liability Partnership (LLP) is similar to a General Partnership except that normally a partner doesn’t have personal liability for the negligence of another partner. This business structure is used most by professionals, such as accountants and lawyers. Filing with the Washington Secretary of State is required.</p>
						</div>
						
						<div class="inpage-mid-text">
							<h3 class="bold">Limited Liability Limited Partnership (LLLP)</h3>
							<p>A Limited Liability Limited Partnership is a Limited Partnership that chooses to become an LLLP by including a statement to that effect in its certificate of limited partnership. This type of business structure may shield general partners from liability for obligations of the LLLP. Filing with the Washington Secretary of State is required.</p>
						</div>
						
						<div class="inpage-mid-text">
							<h3 class="bold">Corporation</h3>
							<p>A Corporation is a more complex business structure. A corporation has certain rights, privileges, and liabilities beyond those of an individual. Doing business as a corporation may yield tax or financial benefits, but these can be offset by other considerations, such as increased licensing fees or decreased personal control. Corporations may be formed for profit or nonprofit purposes. Filing with the Washington Secretary of State is required.</p>
						</div>
						
						<div class="inpage-mid-text">
							<h3 class="bold">Nonprofit Corporation</h3>
							<p>A Nonprofit Corporation is a legal entity and is typically run to further an ideal or goal rather than in the interests of profit. Many nonprofits serve the public interest, but some engage in private sector activities. If your nonprofit organization is, or plans to, raise funds from the public, it may also be required to register with the Charities Program of the Washington Secretary of State. Charitable activities may require additional registration. Contact the Office of the Secretary of State for more information.</p>
						</div>
						
						<div class="inpage-mid-text">
							<h3 class="bold">Limited Liability Company (LLC)</h3>
							<p>A Limited Liability Company (LLC) is formed by 1 or more individuals or entities through a special written agreement. The agreement details the organization of the LLC, including provisions for management, assignability of interests, and distribution of profits and losses. LLCs are permitted to engage in any lawful, for-profit business or activity other than banking or insurance. Filing with the Washington Secretary of State is required.</p>
						</div>
						
						<div class="inpage-mid-text">
							<h3 class="bold">Massachusetts Trust</h3>
							<p>A Massachusetts Trust is an incorporated business with the property being held and managed by the trustees for the shareholders. The trustees are considered employees since they work for the trust. Filing with the Washington Secretary of State is required.</p>
						</div>
						
						<div class="inpage-mid-text">
							<h3 class="bold">Trust</h3>
							<p>A Trust is a legal relationship in which one person, called the trustee, holds property for the benefit of another person, called the beneficiary.</p>
						</div>
						
						<div class="inpage-mid-text">
							<h3 class="bold">Joint Venture</h3>
							<p>A Joint Venture is formed for a limited length of time to carry out a business transaction or operation.</p>
						</div>
						
						<div class="inpage-mid-text">
							<h3 class="bold">Tenants in Common</h3>
							<p>A Tenants in Common allows 2 or more people to occupy the same business while retaining separate identities in regard to assets or liabilities resulting from business activities.</p>
						</div>
						
						<div class="inpage-mid-text">
							<h3 class="bold"> Municipality </h3>
							<p>A Municipality is a public corporation established as a subdivision of a state for local governmental purposes.</p>
						</div>
						
						<div class="inpage-mid-text">
							<h3 class="bold"> Association </h3>
							<p>An Association is an organized group of people who share in a common interest, activity, or purpose.</p>
						</div>
						
				


</div>




</div>

</div>
