<?php
/**
 * Created by PhpStorm.
 * User: barshan-mce
 * Date: 08-Oct-18
 * Time: 10:43 AM
 */


?>


<html>
<body >

<section class="mid-business">
			<div class="container">
					<div  class="text-center text-primary busi-mid">
				      <h1> Successful Stories </h1>
					</div>
			
			
			</div>
	</section>
	
<div class="container">
			<div class="inpage-mid-text">
					<p>Young entrepreneurs are met with various challenges that sometimes push them beyond their potential to bear. However, it is at times like these that they must remember that Alec Baldwin once said; ‘Success begets success.’ If you are an entrepreneur venturing through a new idea and want to make it big, it is important to keep those who’ve been on a similar journey before you as a reminder. Just the awareness that no one before you made it big while being unscathed can be a comforting thought for all those entrepreneurs going through a rough patch right now. Here are 6 such Paskistani entrepreneurs who never surrendered in the face of challenges and remained steadfast and determined in their desire to make it big:</p>
			</div>
						
		<div class="row">
            <?php

		$postCat = \app\models\PostCategory::find()->all();
    foreach($postCat as $k=>$v) {
        $post = \app\models\Post::find()->where('category = ' . $v->id)->all();
        /*echo '<pre>';
        echo print_r($post);
        echo '</pre>';
        exit;*/
        foreach ($post as $p) {?>
					<div class="col-md-4 pb-25 pt-25">
					<div class="card">
							<img class="card-img-top" src="<?= Yii::$app->homeUrl ?><?= $p['img']?>">
						<div class="card-body">
							<h5><?= $p['name'] ?></h5>
								<p class="card-text"><b class="text-danger"><?= $p['name'] ?></b> <?= $p['post_text']?></p>
						</div>
					</div>
					</div>
        <?php }} ?>

		</div><!-- Close Row-->

		</div><!--Close Row-->

	
<div class="container">
			<div class="text-center text-primary bold head2">
					<h2 class="center-align primary bold " style="padding-bottom: 20px">How to be Successful</h2>
			</div>
			
			<div class="inpage-mid-text">
							<h3 class="bold">1.Disciplined</h3>
							<p>These individuals are focused on making their businesses work, and eliminate any hindrances or distractions to their goals. They have overarching strategies and outline the tactics to accomplish them. Successful entrepreneurs are disciplined enough to take steps every day toward the achievement of their objectives.</p>
			</div>
			
			<div class="inpage-mid-text">
							<h3 class="bold">2. Confidence</h3>
							<p>The entrepreneur does not ask questions about whether they can succeed or whether they are worthy of success. They are confident with the knowledge that they will make their businesses succeed. They exude that confidence in everything they do.</p>
			</div>
			
			<div class="inpage-mid-text">
							<h3 class="bold">3. Open Minded</h3>
							<p>Entrepreneurs realize that every event and situation is a business opportunity. Ideas are constantly being generated about workflows and efficiency, people skills and potential new businesses. They have the ability to look at everything around them and focus it toward their goals.</p>
			</div>
			
			<div class="inpage-mid-text">
							<h3 class="bold">3. Cooperative</h3>
							<p>A cooperative is a business organization owned by a group of individuals and is operated for their mutual benefit. The persons making up the group are called members. Cooperatives may be incorporated or unincorporated.
            Some examples of cooperatives are: water and electricity (utility) cooperatives, cooperative banking, credit unions, and housing cooperatives.</p>
			</div>

</div>
</body>
</html>
