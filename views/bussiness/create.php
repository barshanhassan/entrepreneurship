<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Bussiness */

$this->title = 'Create Bussiness';
$this->params['breadcrumbs'][] = ['label' => 'Bussinesses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bussiness-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
