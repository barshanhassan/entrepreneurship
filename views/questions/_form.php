<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Questions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="questions-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'question')->textInput(['maxlength' => true]) ?>


    <?php /*echo  $form->field($model, 'q_types_id')->dropDownList(ArrayHelper::map(\app\models\QuestionType::find()->all(),'id','name'),['prompt'=>'Select Type']);
    */?>

    <?=
    $form->field($model, 'q_types_id', [
            'inputOptions' => ['class' => 'form-control '],])->dropDownList(app\models\Questions::getHierarchy(),['prompt'=>'Select...']);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

