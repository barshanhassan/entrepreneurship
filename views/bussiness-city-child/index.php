<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BussinessCityChildSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bussiness City Children';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
<div class="bussiness-city-child-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create  City Area', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			
			['attribute' => 'bussiness_city_id',
			'value' => 'bussinessCity.bussiness_name',
			],
			
            
			
            'city_child_name',
			
			['attribute' => 'bussiness_name_id',
			
			'value' => 'bussinessName.bussiness_name'
			],
			
            
			
			'latitude',
			
            'longitude',
			
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
</div>