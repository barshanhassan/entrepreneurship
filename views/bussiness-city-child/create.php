<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BussinessCityChild */

$this->title = 'Create City Area';
$this->params['breadcrumbs'][] = ['label' => 'Bussiness City Children', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
<div class="bussiness-city-child-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>