<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\BussinessCityChild */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bussiness-city-child-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'city_child_name')->textInput(['maxlength' => true]) ?>


    <?=$form->field($model,'bussiness_city_id')->dropDownList(ArrayHelper::map(\app\models\BussinessCity::find()->all(),'id','bussiness_name'),['prompt'=> 'Select Main City'])  ?>




    <?= $form->field($model, 'bussiness_name_id')->dropDownList(ArrayHelper::map(\app\models\BussinessName::find()->all(),'id', 'bussiness_name'),['prompt' => 'Select Product'])   ?>

    <?= $form->field($model, 'latitude')->textInput() ?>

    <?= $form->field($model, 'longitude')->textInput() ?>



    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
