<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BussinessCity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bussiness-city-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'bussiness_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'latitude')->textInput() ?>

    <?= $form->field($model, 'langitutde')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
