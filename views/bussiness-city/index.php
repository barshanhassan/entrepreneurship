<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BussinessCitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bussiness Cities';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
<div class="bussiness-city-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Bussiness City', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'bussiness_name',
            'latitude',
            'langitutde',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
</div>