<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BussinessCity */

$this->title = 'Create Bussiness City';
$this->params['breadcrumbs'][] = ['label' => 'Bussiness Cities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
<div class="bussiness-city-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>