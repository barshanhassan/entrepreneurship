<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DailyCity */
?>
<div class="container-fluid">
<?php
$this->title = 'Create Daily City';
$this->params['breadcrumbs'][] = ['label' => 'Daily Cities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daily-city-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>