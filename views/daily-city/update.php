<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DailyCity */

$this->title = 'Update Daily City: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Daily Cities', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="daily-city-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
