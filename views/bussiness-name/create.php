<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BussinessName */

$this->title = 'Create Product Name';
$this->params['breadcrumbs'][] = ['label' => 'Bussiness Names', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
<div class="bussiness-name-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>