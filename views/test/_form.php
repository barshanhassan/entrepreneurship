<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Feedback */
/* @var $form yii\widgets\ActiveForm */
?>


<?php $form = ActiveForm::begin(); ?>

<div class="test-form">

    <div class="row col-md-12">
        <div class="col-lg-12">
            <table class="table table-bordered table-responsive table-condensed text-center">
                <thead>
                    <tr >
                        <th style="text-align: center" >Categories</th>
                        <th style="text-align: center" >Questions</th>
                        <?php $ansoption = \app\models\AnswerOptions::find()->all(); //echo '<pre>'; print_r($revtype);
                        foreach($ansoption as $ans){
                            ?>
                            <th ><?= $ans->options?></th>
                        <?php
                        }
                        ?>
                    </tr>
                </thead>
                <tbody>
                <?php $type = \app\models\QuestionType::find()->all();

                foreach($type as $k=>$v){

                    $question = \app\models\Questions::find()->where('q_types_id = '.$v->id)->all();
                    ?>

                <?php

                    foreach($question as $d=>$p){

                        ?>
                        <input type="hidden" value="<?php echo $v->id?>" name="type[<?= $p->id ?>]">
                    <tr>
                        <?php if($question[$d]['q_types_id']!=$question[$d-1]['q_types_id']){
                            ?>

                            <td class="centercontent" rowspan="<?= count($question)?>"><?= $v->name ?></td>

                        <?php
                        } ?>
                            <td><?= $p->question ?></td>

                        <input type="hidden" value="<?php echo $p->id?>" name="question[<?= $p->id ?>]">
                        <?php $ansoption = \app\models\AnswerOptions::find()->all(); //echo '<pre>'; print_r($revtype);
                        foreach($ansoption as $m){
                            ?>
                            <td>
                                <div class="radio">
                                    <label><input type="radio" id='subcatradio_<?= $m->id ?>' value="<?= $m->id ?>" name="subcatradio[<?= $p->id ?>]"></label>
                                </div>
                            </td>
                        <?php
                        }
                        ?>
                    </tr>
                <?php } ?>
                <?php
                }
                ?>
                </tbody>
            </table>
        </div>

</div>
</div>

<div class="form-group">
    <?= Html::submitButton('Submit', ['class' => 'btn btn-success']) ?>
</div>
<?php ActiveForm::end(); ?>
<style>
    .radio label, .checkbox label {
        width: 100%;
        height: 100%;
    }
</style>